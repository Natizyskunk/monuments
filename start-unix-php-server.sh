#!/bin/bash
readonly localhost=127.0.0.1
readonly external_bind=0.0.0.0
readonly port=8000

if which xdg-open > /dev/null
then
  xdg-open http://localhost:$port
elif which gnome-open > /dev/null
then
  gnome-open http://localhost:$port
fi

### Start php web server on localhost on port 8000.
# php -S $localhost:$port

### Start php web server on all interfaces on port 8000.
php -S $external_bind:$port