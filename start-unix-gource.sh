#!/bin/bash
## Define const & parameters.
readonly title=Monuments-git-vizualiser
readonly fullScreen=-f
readonly screenRes=-1280x720
readonly outputFramerate=60
readonly autoSkipSeconds=1
readonly secondsPerDay=2
readonly maxFiles=0
readonly maxFileLag=0.5
readonly cameraMode=overview
readonly crop=vertical
readonly padding=1.5
readonly captionFile=gource.caption
readonly outputPpmStream=gource.ppm
readonly configFile=gource.conf
readonly logCommand=git
readonly logFormat=git

## Start Gource with parameters.
# gource --title $title $screenRes --output-framerate $outputFramerate --disable-bloom --auto-skip-seconds $autoSkipSeconds --seconds-per-day $secondsPerDay --stop-at-end --key --max-files $maxFiles --max-file-lag $maxFileLag --camera-mode $cameraMode --crop $crop --padding $padding --caption-file $captionFile --output-ppm-stream $outputPpmStream --save-config $configFile
gource --load-config $configFile
# gource --save-config $configFile

## Logs:
# gource --log-command $logCommand --log-format $logFormat