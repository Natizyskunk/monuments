<?php
if (route('accueil') || route('')) {
	$controller = _CTRL_.'Accueil/index.php';
    include_once($controller);
}
elseif (route('form-oeuvres')) {
	$controller = _CTRL_.'Formulaires/Oeuvres/index.php';
    include_once($controller);
}
elseif (route('form-category')) {
	$controller = _CTRL_.'Formulaires/Category/index.php';
    include_once($controller);
}
elseif (route('form-campus')) {
	$controller = _CTRL_.'Formulaires/Campus/index.php';
    include_once($controller);
}
elseif (route('map')) {
	$controller = _CTRL_.'Map/index.php';
    include_once($controller);
}
elseif (route('patrimoine')) {
	$controller = _CTRL_.'Patrimoine/index.php';
    include_once($controller);
}
elseif (route('patrimoine-([0-9]+)')) {
    $controller = _CTRL_.'Patrimoine/oeuvre_select.php';
    include_once($controller);
}
elseif (route('connexion')) {
	$controller = _CTRL_.'Connexion/index.php';
    include_once($controller);
}
elseif (route('deconnexion')) {
	$controller = _CTRL_.'Deconnexion/index.php';
    include_once($controller);
}
elseif (route('create-password')) {
	$controller = _CTRL_.'Connexion/create_password.php';
    include_once($controller);
}
elseif (route('password-reset')) {
	$controller = _CTRL_.'Connexion/password_reset.php';
    include_once($controller);
}
elseif (route('admin')) {
	$controller = _CTRL_.'Admin/index.php';
    include_once($controller);
}
elseif (route('admin/new-user')) {
	$controller = _CTRL_.'Admin/New_user/index.php';
    include_once($controller);
}
elseif (route('error-404')) {
	// $controller = _CTRL_.'Error_404/index.php';
	// include_once($controller);
	$smarty->display(_TPL_.'Error_404/index.html');
}
else {
	header('Location: /error-404');
}
//>>> ROUTER START
?>