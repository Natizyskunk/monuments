# Monuments

### Comment installer le projet.

#### A) Installer les prérequis.
1. Installer composer.
2. Installer node.js (npm).
3. Cloner le projet dans /var/www/html sur Linux depuis le repository Gitlab.
4. Se rendre dans le repertoire racine du projet (là où se trouve le fichier index.php).
5. Ouvrir un terminal à cet emplacement et entrer la commande : `composer install` puis la commande `npm install`.
6. Créer une base de données nommée 'monuments_admin' grâce à la commande SQL: `CREATE DATABASE monuments_admin CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`.
7. Editer ensuite les fichiers propel.php et propel.php.dist avec les bons parametres. (user: monuments; pass: uha40monuments;)
```bash
sudo nano /var/www/html/propel.php
sudo nano /var/www/html/propel.php.dist
```
8. Puis ouvrir un terminal au même endroit et entrer les commandes suivantes:
```bash
vendor/bin/propel sql:build --overwrite
vendor/bin/propel model:build
composer dump-autoload
vendor/bin/propel config:convert
vendor/bin/propel migrate
	```
9. Après, changer les droits pour autoriser smarty à écrire dans le dossier des vues (templates_c).
```bash
sudo chmod a+w /var/www/html/view
# Si une erreur smarty s'affiche concernant la fonction Utime(); il faut modifier les droits d'acces au dossier racine du projet.
sudo chmod a+w /var/www/html
```
10. Et enfin Installer et configurer le firewall et ouverture de ports.
```bash
sudo apt install -y ufw
sudo ufw allow 8000
sudo ufw allow 8000/tcp
```

#### B) Lancer l'environnement de développement.
###### B.1) Lancement du projet en grâce au serveur de developpement interne à PHP.
vous pouvez vous en passer et opter plutôt pour l'utilsation du script 'start-<distro>-php-server' situé à la racine du projet. <br>
Il permet d'utiliser le seveur de développement interne à PHP. <br>
Pour ce faire vous devez d'abord ouvrir le port utiliser par le script dans votre firewall (port: 8000). <br>
1. Installation et configuration firewall et ouverture de ports. <br>
```bash
sudo apt install -y ufw
sudo ufw allow 8000
sudo ufw allow 8000/tcp
```
2. Ensuite, il faut rendre le script executable avec la commande :
```bash
cd /var/www/html
sudo chmod +x start-unix-php-server.sh
```
3. Enfin vous pouvez lancer le script avec la commande :
```bash
./start-unix-php-server.sh
```
le script va lancer le serveur web de développement interne et va automatiquement vous ouvrir une nouvelle page sur votre navigateur avec l'url http://localhost:8000 . <br>
Si tout c'est bien passé, vous devriez atterir sur la page d'accueil du site du Patrimoine du SUAC. <br>
Note: Quand vous avez fini le développement, vous pouvez taper la combinaison de touches « CTRL + C » afin de stoper le serveur de développement interne et fermer le terminal.

###### B.2) Lancement du projet via virtualHost et fichier .htaccess (recommandé).
1. Configuration du virtualHost apache (http.conf ou 00-default.conf la plupart du temps):
```bash
sudo nano /etc/apache2/sites-available/http.conf
#ou si http.conf n'existe pas.
sudo nano /etc/apache2/sites-available/000-default.conf # devrait être celui par défaut la plupart du temps.
```
Avec la configuration suivante:
```conf
<VirtualHost *:80>
	# ServerAdmin admin@mondomaine.fr
	DocumentRoot /var/www/html/html
	ServerName uha40monuments.test
	ServerAlias *.html.test

	<Directory "/var/www/html/html/">
		AllowOverride All
		Require all granted
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	DirectoryIndex index.html index.php index.xhtml index.htm
</VirtualHost>
```
2. Après avoir sauvegardé vos modifications, il faut éditer le fichier hosts:
```bash
	sudo nano /etc/hosts
```
Et y ajouter le ServerName indiqué dans le VirtualHost:
```
...
127.0.0.1   uha40monuments.test
...
```
3. Vérification du fichier .htaccess situé à la racine du projet.
```bash
sudo nano /var/www/html/.htaccess
```
```.htaccess
<<<< FILE START >>>>
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^.*$ index.php [L,QSA]
<<<< FILE END >>>>
```
4. Enfin vous pouvez relancer votre serveur apache.

#### C) Changer les droits pour autoriser smarty à écrire dans le dossier des vues (templates_c).
```bash
sudo chmod a+w /var/www/html/view
sudo chmod a+w /var/www/html/assets/img
# Si une erreur smarty s'affiche concernant la fonction Utime(); il faut modifier les droits d'acces au dossier racine du projet.
sudo chmod a+w /var/www/html
```

#### D) Lancer Sass compiler (Optionnel).
###### live compiler scss->css
Live Sass compiler permet de compiler automatiquement tous les fichiers .scss en fichiers .css au moment de la sauvegarde d'une modification « CTRL + S ». <br>
1. Se rendre dans le repertoire racine du projet (là où se trouve le fichier index.php).
2. Ouvrir un terminal à cet emplacement et entrer la commande : `npm run sass`.
3. Ouvrir votre éditeur ou IDE préféré.
4. Vous pouvez mtn coder votre design dans les fichiers .scss se trouvant dans le dossier `assets/css/

**Attention:** Ne pas coder directement dans les fichier css car ils sont auto-compilé par Sass et sauront donc écrasés **!!**

#### E) VOILA!
Vous pouvez dès à présent vous connecter au site de développement en ouvrant un navigateur et en renseignant l'url 'http://'
Vous êtes maintenant prêt à coder dans votre éditeur ou IDE préféré.

#### F) BONUS - Installer phpmyadmin et le proteger via .htpaswd.
- https://www.vultr.com/docs/how-to-install-and-secure-phpmyadmin-on-ubuntu-14-04-and-16-04
- (SSL): https://askubuntu.com/questions/938398/how-to-secure-phpmyadmin
