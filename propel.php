<?php
$db = 'monuments_admin';
return [
    'propel' => [
        'database' => [
            'connections' => [
                $db => [
					'adapter' => 'mysql',
					// 'classname'  => 'Propel\Runtime\Connection\DebugPDO',
					// 'classname'  => 'Propel\Runtime\Connection\ConnectionWrapper',
					'dsn' => 'mysql:host=localhost;port=3306;dbname='.$db,
                    'user' => 'root',
					'password' => '',
					/* 'attributes' => [
                        // 'isSlowOnly' => true
                    ], */
                    'settings' => [
						'charset' => 'utf8mb4',
						'queries' => [
							'utf8mb4' => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci, COLLATION_CONNECTION = utf8mb4_unicode_ci, COLLATION_DATABASE = utf8mb4_unicode_ci, COLLATION_SERVER = utf8mb4_unicode_ci'
						]
                    ]
                ]
            ]
        ],
		'runtime' => [
			'defaultConnection' => $db,
			'connections' => [$db],
			'log' => [
                'defaultLogger' => [
                    'type' => 'stream',
                    'path' => '/propel/log/propel.log',
                    'level' => 300
                ],
                $db => [
                    'type' => 'stream',
                    'path' => '/propel/log/propel_monuments_admin.log',
                ]
			],
			'profiler' => [
                'classname' => '\Propel\Runtime\Util\Profiler',
                'slowTreshold' => 0.1,
                'details' => [
                    'time'    => [
                        'precision' => 3,
                        'pad' => 8
                    ],
                    'mem'    => [
                        'precision' => 3,
                        'pad' => 8
                    ]
                ]
            ]
		],
		'generator' => [
			'defaultConnection' => $db,
			'connections' => [$db]
		]
    ]
];
