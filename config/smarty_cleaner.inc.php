<?php
// Nettoyage de Smarty.
$smarty->clearAllCache(3600); //! Efface tous les fichiers du cache vieux d'une heure.
$smarty->clearConfig(); //! Efface toutes les variables de config assignées au fichier(s) <filename>.conf
$smarty->clearAllAssign(); //! Efface toutes les variables assignées à Smarty.
?>