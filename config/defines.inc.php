<?php
// Si on a pas ces infos, rien ne peut fonctionner : die().
if (!isset($_SERVER['DOCUMENT_ROOT'])) {
	die();
}


//>>> FUNCTIONS START
// Définition de la fonction startsWith().
function startsWith($haystack, $needle) {
	$length = strlen($needle);
	return (substr($haystack, 0, $length) === $needle);
}

function str_replace_first($string, $match, $replace) {
    $pos = strpos($string, $match);

	if ($pos !== false) {
		$newstring = substr_replace($string, $replace, 0, strlen($match));
	}

	return $newstring;
}

function getPage() {
	GLOBAL $url;
	GLOBAL $page;

	if(preg_match('/^\A.*\z$/', _ROUTE_, $matches)) {
		$url 						= $matches[0];
		$urlExplodeBySlash 			= explode('/', $url);
		$urlExplodeByQuestionMark 	= explode('?', $url);

		if (isset($urlExplodeByQuestionMark[0])) {
			$page = $urlExplodeByQuestionMark[0];
		}

		if (isset($urlExplodeByQuestionMark[1])) {
			$urlParameters = $urlExplodeByQuestionMark[1];
		}
	}

	return $url;
	return $page;
}

function route($route) {
	GLOBAL $page;
	GLOBAL $matches;

	$replacedRoute 	= str_replace("/", "\/", '^\A'.$route.'\z$');
	$match 			= preg_match('/'.$replacedRoute.'/', $page, $matches);

	return $match;
	return $matches;
}
//<<< FUNCTIONS END

//>>> CONSTANTS START
// Définition du lien actuel de la page visité.
define('_ROUTE_', str_replace_first($_SERVER["REQUEST_URI"], '/', '')); // define('_ROUTE_', $_SERVER['REQUEST_URI']);

// Définition de la racine du site.
define('_PATH_', $_SERVER['DOCUMENT_ROOT'].'/');

// Définition de l'hôte.
define('_HOST_', "http://".$_SERVER['HTTP_HOST']."/");

// Définition du dossier des Controleurs.
define('_CTRL_', _PATH_ . 'controllers/');

// Définition du dossier des Configs MVC.
define('_CONFIG_', _PATH_ . 'config/');

// Définition des assets.
define('_ASSETS_', _PATH_ . 'assets/');

// Définition du dossier des TPL.
define('_TPL_', _PATH_ . 'view/templates/');

// Définition du dossier des TPL compilés.
define('_TPLC_', _PATH_ . 'view/templates_c/');

// Définition du dossier des Configs Smarty.
define('_SMARTYCONFIG_', _PATH_ . 'view/configs/');

// Définition du dossier des caches Smarty.
define('_CACHE_', _PATH_ . 'view/cache/');

// Définition du dossier htdocs Smarty.
define('_HTDOCS_', _PATH_ . 'view/htdocs/');

// Définition du dossier des Syslugins Smarty.
define('_SYSPLUGINS_', _PATH_ . 'view/sysplugins/');

// Définition du dossier des Plugins Smarty.
define('_PLUGINS_', _PATH_ . 'view/plugins/');
//<<< CONSTANTS END


//>>> VARIABLES START
$url  = '';
$page = '';
//<<< VARIABLES END
?>