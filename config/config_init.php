<?php
// DEBUGING
// ini_set('display_errors', 'on'); //! Changer à "off" quand passage en prod.
// ini_set('error_reporting', E_ALL); //! Changer à "0" quand passage en prod.
ini_set('display_errors', 'off'); //! Changer à "off" quand passage en prod.
ini_set('error_reporting', 0); //! Changer à "0" quand passage en prod.

// Définition du fuseau horaire.
date_default_timezone_set('Europe/Paris');

// Initialisation de la session.
session_start();
header("Cache-Control: no-cache");

// Chargement Smarty et Defines
require_once(__DIR__.'/defines.inc.php');
require_once(_PATH_.'vendor/autoload.php');

// Initialisation Propel
require_once(_PATH_.'generated-conf/config.php');

// Initialisation Smarty
$smarty = new Smarty();
$smarty->compile_check  = true;
$smarty->force_compile  = false;
$smarty->caching        = 0; //! Le cache est désactivé.
$smarty->cache_lifetime = 0; //! Le cache n'est jamais (re)généré (paramètre en secondes).
// $smarty->debugging      = true; //! Décommenter cette ligne pour accéder à la console de debugging de Smarty.
$smarty->setTemplateDir(_TPL_);
$smarty->setCompileDir(_TPLC_);
$smarty->setConfigDir(_SMARTYCONFIG_);
$smarty->setCacheDir(_CACHE_);
$smarty->setPluginsDir(_PLUGINS_);

$aMessageError 	 = array();
$aMessageSuccess = array();

//>>> ROUTER START
getPage();
// Définition de la page du site du site.
define('_PAGE_', $page);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <?php $smarty->display(_TPL_.'head.html'); ?>
</head>