<?php
// Nettoyage de la session.
$_SESSION = array();
session_unset();
session_destroy();
session_abort();
session_write_close();

// Suppression des cookies de connexion automatique.
setcookie('login', '', time()-1);
setcookie('pass_hash', '', time()-1);
unset($_COOKIE['login']);
unset($_COOKIE['pass_hash']);
?>