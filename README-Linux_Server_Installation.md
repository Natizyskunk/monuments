0) Mettre à jour ses dependences et paquets.

```
sudo apt update -y && apt dist-upgrade -y && apt autoremove --purge
sudo reboot
```

1) Installer LAMP avec Apache2, Mysql 5.7 (ou mariadb) et PHP-7.2.

Si LAMP déjà installer comme sur les VPS Vultr par exemple, il suffit à ce moment là de mettre à jour PHP vers 7.2: https://murviel-info-beziers.com/mettre-a-jour-php-7-2-ubuntu/
```bash
sudo systemctl restart php7.2-fpm
sudo apt update -y && apt dist-upgrade -y && apt autoremove --purge
```

2) Configurer mysql.

se connecter ensuite en ligne de commande à mysql:
```bash
mysql -u root -p
# pass: monuments
```
```SQL
CREATE USER 'monuments'@'localhost' IDENTIFIED BY 'uha40monuments';
GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';
GRANT ALL PRIVILEGES ON * . * TO 'root'@'localhost';
exit
```
```bash
sudo systemctl restart mysql
sudo systemctl enable mysql
sudo systemctl restart mysqld
sudo systemctl enable mysqld
sudo systemctl restart apache2
sudo systemctl enable apache2
mysql -u root -p
# pass: monuments
```
```SQL
CREATE DATABASE monuments_admin CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
exit
```
```bash
sudo systemctl restart mysql
sudo systemctl restart mysqld
```

3) Installation des paquets nécessaire à la mise en place et la configuration de l'environnement.

```bash
sudo apt install -y --no-install-recommends libicu-dev libmemcached-dev libz-dev libpq-dev libjpeg-dev libfreetype6-dev libssl-dev libmcrypt-dev libxml2-dev libbz2-dev curl git gnupg nodejs ssh-client
curl -sL https://deb.nodesource.com/setup_11.x | bash -
sudo apt install -y nodejs
sudo apt install zip
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
```

4) Création d'une clé SSH et ajout des hôtes connues.

```bash
sudo mkdir /root/.ssh/
# créer une clé privée et l'enregistrer dans ~/.ssh/id_rsa:
sudo ssh-keygen -t rsa -C "testServer" -b 4096
# Quand on vous demande un mot de passe, laisser vide !
cat ~/.ssh/id_rsa
sudo export SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)"
sudo chmod 600 ~/.ssh/id_rsa
ssh-keyscan 145.239.32.253 >> ~/.ssh/known_hosts
# Ajouter la nouvelle clé publique ssh sur son compte gitlab uha.
# http://145.239.32.253/help/ssh/README
```

5) Clonage du projet.

```bash
ls -l /var/www
ls -l /var/www/html
sudo git clone git@145.239.32.253:UHA40/monument_ADMIN.git /var/www/html
cd /var/www/html
```

6) Installation du projet.

```bash
npm install
# Editer les fichiers propel.php et propel.php.dist avec les bons parametres. (user: monuments; pass: uha40monuments;)
sudo nano /var/www/html/propel.php
sudo nano /var/www/html/propel.php.dist
pwd
composer install
```

7) configuration de l'ORM Propel (connexion à la bdd).

```bash
vendor/bin/propel sql:build --overwrite
vendor/bin/propel model:build
composer dump-autoload
vendor/bin/propel config:convert
```

8) Migration des données vers la bdd.

```bash
vendor/bin/propel migrate
```

9) Changer les droits pour autoriser smarty à écrire dans le dossier des vues (templates_c).

```bash
sudo chmod a+w /var/www/html/view
sudo chmod a+w /var/www/html/assets/img
# Si une erreur smarty s'affiche concernant la fonction Utime(); il faut modifier les droits d'acces au dossier racine du projet.
sudo chmod a+w /var/www/html
```

10) Installation et configuration firewall et ouverture de ports.

```bash
sudo apt install -y ufw
sudo ufw allow 8000
sudo ufw allow 8000/tcp
```

11.a) Lancement du projet en environnement de developpement.

```bash
# !! seulement en environnement de developpement. Utilisation d'un .htaccess en production !!
nohup php -S 0.0.0.0:8000 &
```

11.b) Lancement du projet en environnement de production.

Mise en place d'un fichier .htaccess en production.

12) Activation de l'extension mod_rewrite.

```bash
sudo a2enmod rewrite
sudo systemctl restart apache2
```

13) Configuration du virtualHost apache (http.conf ou 00-default.conf la plupart du temps).

```bash
sudo nano /etc/apache2/sites-available/http.conf
#ou si http.conf n'existe pas.
sudo nano /etc/apache2/sites-available/000-default.conf # devrait être celui par défaut la plupart du temps.
```
```conf
<<<< FILE START >>>>
<VirtualHost *:80>
    <Directory /var/www/html/>
#         Options -Indexes
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>

    <Files ".ht*">
        Require all denied
    </Files>

    #ServerAdmin webmaster@example.com
    #ServerName host.example.com

    DocumentRoot /var/www/html

    SetEnvIf Request_URI "^/favicon\.ico$" dontlog
    SetEnvIf Request_URI "^/robots\.txt$" dontlog

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

	DirectoryIndex index.html index.php index.xhtml index.htm
</VirtualHost>
<<<< FILE END >>>>
```

Sauvegarder et fermer le fichier.
```bash
sudo systemctl restart apache2
```

14) Vérification du fichier .htaccess.

```bash
sudo nano /var/www/html/.htaccess
```
```.htaccess
<<<< FILE START >>>>
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^.*$ index.php [L,QSA]
<<<< FILE END >>>>
```

15) BONUS: Installer phpmyadmin et le proteger via .htpaswd.

- https://www.vultr.com/docs/how-to-install-and-secure-phpmyadmin-on-ubuntu-14-04-and-16-04
- (SSL): https://askubuntu.com/questions/938398/how-to-secure-phpmyadmin
