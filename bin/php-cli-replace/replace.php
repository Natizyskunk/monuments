<?php
// ------ DEFINES ------

define('_PATH_', dirname(dirname(dirname(__FILE__))));
define('_BIN_', dirname(dirname(dirname(__FILE__))).'/bin');
define('_ASSETS_', dirname(dirname(dirname(__FILE__))).'/assets');

// ------ FUNCTIONS ------

function replace_string_in_file($filename, $string_to_replace, $replace_with){
    $file_content 			= file_get_contents($filename);
    $file_content_chunks 	= explode($string_to_replace, $file_content);
    $file_content 			= implode($replace_with, $file_content_chunks);

	file_put_contents($filename, $file_content);
}

function db_connection(){
	global $answer;
	global $dbHost;
	global $dbPort;
	global $dbName;
	global $dbUser;
	global $dbPass;

	if (PHP_OS == 'WINNT') {
		echo "db host [localhost]: ";
		$dbHost = stream_get_line(STDIN, 1024, PHP_EOL);

		echo "db port [3306]: ";
		$dbPort = stream_get_line(STDIN, 1024, PHP_EOL);

		echo "db name [monuments_admin]: ";
		$dbName = stream_get_line(STDIN, 1024, PHP_EOL);

		echo "db user [root]: ";
		$dbUser = stream_get_line(STDIN, 1024, PHP_EOL);

		echo "db pass []: ";
		$dbPass = stream_get_line(STDIN, 1024, PHP_EOL);
	}
	else {
		$dbHost = readline("db host [localhost]: ");
		readline_add_history($dbHost);

		$dbPort = readline("db port[3306]: ");
		readline_add_history($dbPort);

		$dbName = readline("db name[monuments_admin]: ");
		readline_add_history($dbName);

		$dbUser = readline("db user[root]: ");
		readline_add_history($dbUser);

		$dbPass = readline("db pass[]: ");
		readline_add_history($dbPass);
	}

	if (empty($dbHost)) {
		$dbHost = "localhost";
	}
	if (empty($dbPort)) {
		$dbPort = "3306";
	}
	if (empty($dbName)) {
		$dbName = "monuments_admin";
	}
	if (empty($dbUser)) {
		$dbUser = "root";
	}
	if (empty($dbPass)) {
		$dbPass = "";
	}

	echo "\r\nChecking db connection credentials: \r\n db host: ".$dbHost." \r\n db port: ".$dbPort." \r\n db name: ".$dbName." \r\n db user: ".$dbUser." \r\n db pass: ****** \r\n";

	if (PHP_OS == "WINNT") {
		echo "Are they correct ? [Yes/No]: ";
		$answer = stream_get_line(STDIN, 1024, PHP_EOL);
	}
	else {
		$answer = readline("[Yes/No]: ");
		readline_add_history($answer);
	}

	return $answer;
	return $dbHost;
	return $dbPort;
	return $dbName;
	return $dbUser;
	return $dbPass;
}

// ------ CODE ------

db_connection();

$propelDbConn 		= _PATH_."/propel.php";
$propelSchema 		= _PATH_."/propel/schema/schema.xml";
$propelMigrationDir = _PATH_."\/generated-migrations/";
/* $bigDumpDbConn 		= _BIN_."/php-cli-replace/bigdump.php";
$sqlDbConn 			= _ASSETS_."/sql/monuments_db.sql"; */
$answers 			= array("YES", "Yes", "yes", "Y", "y");


while (!in_array($answer, $answers)) {
	echo "ERROR \r\n";
	db_connection();
}

if (in_array($answer, $answers)) {
	$old_propel_dbHost = "mysql:host=localhost;";
	$old_propel_dbPort = "port=3306;";
	$old_propel_dbName = '$db = \'monuments_admin\';';
	$old_propel_dbUser = "'user' => 'root'";
	$old_propel_dbPass = "'password' => ''";

	$new_propel_dbHost = "mysql:host=".$dbHost.";";
	$new_propel_dbPort = "port=".$dbPort.";";
	$new_propel_dbName = '$db = \''.$dbName.'\';';
	$new_propel_dbUser = "'user' => '".$dbUser."'";
	$new_propel_dbPass = "'password' => '".$dbPass."'";

	$old_propelSchema_dbName = 'database name="monuments_admin"';
	$new_propelSchema_dbName = 'database name="'.$dbName.'"';

	$old_propelMigration_AdapterConn 	= "getAdapterConnection('monuments_admin');";
	$old_propelMigration_dbName 		= "'monuments_admin' => '";

	$new_propelMigration_AdapterConn 	= "getAdapterConnection('".$dbName."');";;
	$new_propelMigration_dbName 		= "'".$dbName."' => '";


	// Replace propel.php config.
	replace_string_in_file($propelDbConn, $old_propel_dbHost, $new_propel_dbHost);
	replace_string_in_file($propelDbConn, $old_propel_dbPort, $new_propel_dbPort);
	replace_string_in_file($propelDbConn, $old_propel_dbName, $new_propel_dbName);
	replace_string_in_file($propelDbConn, $old_propel_dbUser, $new_propel_dbUser);
	replace_string_in_file($propelDbConn, $old_propel_dbPass, $new_propel_dbPass);

	// Replace propel schema.xml db name.
	replace_string_in_file($propelSchema, $old_propelSchema_dbName, $new_propelSchema_dbName);


	$files  = scandir($propelMigrationDir);
	$lenght = count($files);

	for ($i = 0; $i < $lenght; $i++) {
		if (preg_match('/(.*).php/', $files[$i])) {
			echo "migration file n° ".$i.": ".$files[$i]."\r\n";
			// Replace propel migration files db name.
			replace_string_in_file($propelMigrationDir.$files[$i], $old_propelMigration_AdapterConn, $new_propelMigration_AdapterConn);
			replace_string_in_file($propelMigrationDir.$files[$i], $old_propelMigration_dbName, $new_propelMigration_dbName);
		}
	}

	// ------ DB CREATION ------
	try {
		$conn = new PDO("mysql:host=$dbHost", $dbUser, $dbPass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "CREATE DATABASE IF NOT EXISTS $dbName DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
		$conn->exec($sql);

		echo "Database created successfully (not created if already exist)";
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
	$conn = null;
}