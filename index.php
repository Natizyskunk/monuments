<?php
// Initialisation de l'environnement.
include('./config/config_init.php');
?>

<body>

<?php
// Affiche le header sur chaque page.
$smarty->display(_TPL_ . 'header.html');

// Chargement du routeur php.
include_once(_PATH_."router.php");

// Affiche le footer sur chaque page.
$smarty->display(_TPL_.'footer.html');

// Chargement du routeur php.
$smarty->display(_TPL_.'scripts_js.html');
?>


</body>
</html>

<?php
// Initialisation du nettoyage Smarty.
include(_CONFIG_.'smarty_cleaner.inc.php');
?>