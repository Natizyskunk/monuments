
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- campus_site
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `campus_site`;

CREATE TABLE `campus_site`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `campus` VARCHAR(255) NOT NULL,
    `latitude` VARCHAR(255) NOT NULL,
    `longitude` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `campus` (`campus`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `categorie` VARCHAR(255) NOT NULL,
    `image` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `categorie` (`categorie`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- oeuvres
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `oeuvres`;

CREATE TABLE `oeuvres`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nom` VARCHAR(255) NOT NULL,
    `artiste` VARCHAR(255) NOT NULL,
    `annee` VARCHAR(255) NOT NULL,
    `categorie` INTEGER NOT NULL,
    `latitude` VARCHAR(255) NOT NULL,
    `longitude` VARCHAR(255) NOT NULL,
    `campus` INTEGER NOT NULL,
    `description` TEXT NOT NULL,
    `image` VARCHAR(255) NOT NULL,
    `droits_auteur` VARCHAR(255) NOT NULL,
    `date_creation` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `categorie` (`categorie`),
    INDEX `campus` (`campus`),
    CONSTRAINT `campus`
        FOREIGN KEY (`campus`)
        REFERENCES `campus_site` (`id`),
    CONSTRAINT `categorie`
        FOREIGN KEY (`categorie`)
        REFERENCES `category` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `pseudo` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `rights` INTEGER NOT NULL,
    `password_changed` INTEGER NOT NULL,
    `date_creation` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `rights` (`rights`),
    CONSTRAINT `rights`
        FOREIGN KEY (`rights`)
        REFERENCES `users_rights` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users_rights
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users_rights`;

CREATE TABLE `users_rights`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `rights` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `rights` (`rights`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
