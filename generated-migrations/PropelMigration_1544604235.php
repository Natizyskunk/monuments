<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1544604235.
 * Generated on 2018-12-12 08:43:55
 */
class PropelMigration_1544604235
{
	public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
		// add the post-migration code here
		$pdo = $manager->getAdapterConnection('monuments_admin');

		$sql = "INSERT INTO `campus_site` (`id`, `campus`, `latitude`, `longitude`) VALUES
				(1,'UHA Colmar', '48.07243015429286', '7.353544235229493'),
				(2, 'UHA Mulhouse', '47.732028', '7.313837');";

		$stmt = $pdo->prepare($sql);
		$stmt->execute();

		$sql = "INSERT INTO `category` (`id`, `categorie`, `image`) VALUES
				(1, 'architecture remarquable', '/assets/img/icons/map/category/architecture_remarquable.png'),
				(2, 'installation éphémère', '/assets/img/icons/map/category/installation_ephemere.png'),
				(3, 'peinture', '/assets/img/icons/map/category/peinture.png'),
				(4, 'résidence d''artiste', '/assets/img/icons/map/category/residence_artiste.png'),
				(5, 'sculpture', '/assets/img/icons/map/category/sculpture.png');";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();

		$sql = "INSERT INTO `oeuvres` (`id`, `nom`, `artiste`, `annee`, `categorie`, `latitude`, `longitude`, `campus`, `description`, `image`, `droits_auteur`, `date_creation`) VALUES
				(1, 'Bibliothèque Universitaire IUT Colmar', 'Ville de Colmar', '2000', 1, '48.0775268', '7.371303399999988', 1, 'Construite autour de l''ancienne cheminée de briques, mémoire du site industriel qui donne une valeur patrimoniale au nouvel espace,\r\nla bibliothèque et son totem sont le signal inattendu qui manifeste la présence du site universitaire.\r\n\r\nFace à elle, un soubassement en pierres-moellons de grès hérités de l''ancienne usine, structure l''édification des bâtiments de l''IUT en rappelant l''activité industrielle passée.\r\n\r\nCes liens architecturaux qui racontent l''histoire du site sont soulignés par une scénographie spécifique.\r\nAinsi, le bâtiment Génie des Télécommunications et Réseaux (GTR) expose sur ses façades de verre le vocabulaire spécifique lié au processus de fabrication des étoffes…\r\n\r\nLes bâtiments de l''IUT sont une forme de \"révélateur\" de l''empreinte de l''ancienne usine, par les rythmes et la répétition des pignons sur murs, la rigueur et la simplicité des formes élémentaires qu''ils déclinent.\r\nIls forment, non pas une nouvelle composition d''ensemble, mais un renouvellement du site, dans une certaine continuité, une certaine mémoire urbaine en relation avec l''activité industrielle passée.', '/assets/img/photos/oeuvres/BU_IUT_Colmar.jpg', 'NULL', '1900-01-01 00:00:00'),
				(2, 'Campus Fonderie (FSESJ)', 'à définir', 'à définir', 1, '47.73887329999999', '7.332372699999951', 2, 'L''Université de Haute-Alsace accueille aujourd''hui près de 9 700 étudiants au travers de plus de 170 formations tant fondamentales que professionnelles.\r\nSituée sur le Campus de la Fonderie à Mulhouse, la Faculté des Sciences Economiques Sociales et Juridiques (FSESJ) s''inscrit d''emblée dans les enjeux du développement économique et social.\r\nElle offre des conditions de travail exceptionnelles et une interface très forte avec les acteurs économiques : intervenants extérieurs, partenariats, recherche, formations professionnalisantes, sont autant...', '/assets/img/photos/oeuvres/Campus_Fonderie _FSESJ.jpg', 'NULL', '1900-01-01 00:00:00'),
				(3, 'Faculté des Sciences et Techniques de l''Université de Haute Alsace', 'Mongiello & Plisson, et Hemmerle', '1997', 3, '47.728541', '7.308516', 2, 'à définir', '/assets/img/photos/oeuvres/FST_UHA.jpg', 'NULL', '1900-01-01 00:00:00'),
				(4, 'Fresque murale', 'Robert Wogensky', '1971', 3, '47.730812', '7.300569', 2, 'Œuvre en lave émaillée.\r\nA enseigné à l''Ecole des Beaux-Arts de Nancy, Ecole Nationale des Arts appliqués de Paris et  également artiste.\r\n\r\nParallèlement aux réalisations murales, Robert Wogensky n''a cessé de pratiquer la peinture sous toutes ses formes et techniques : aquarelle, encre acrylique, peinture à l''huile.\r\n', '/assets/img/photos/oeuvres/Fresque_murale.jpg', 'NULL', '1900-01-01 00:00:00'),
				(5, 'Les Suspendues', 'Sima Khatami et Stéfane Perraud', '2014', 2, '48.0775268', '7.371303399999988', 1, 'Dans le cadre du programme Art Campus, les \"Suspendues\" est une installation sonore et visuelle qui murmure au gré des vents les révoltes d''hier et d''aujourd''hui.\r\nSous cette passerelle sont suspendus des billets métalliques rectangulaires sur lesquels sont imprimés des photos ou des documents de combats et d''engagements de tout un chacun.\r\n\r\nCette œuvre est diffusée par le Théâtre de la Cité internationale dans le cadre du programme \"Art Campus\", avec le soutien du ministère de la Culture et de la Communication, du Centre National des Œuvres Universitaires et Scolaires et en partenariat avec le réseau \"Art-Université-Culture\".', '/assets/img/photos/oeuvres/Les_suspendues.jpg', 'NULL', '1900-01-01 00:00:00'),
				(6, 'Melsass', 'Martin Chramosta', '2016', 4, '48.062956', '7.336291699999947', 1, 'Cet ouvrage vient clore la résidence universitaire de l''artiste suisse, Martin Chramosta organisée conjointement entre la Kunsthalle et le SUAC qui s''est déroulée en 2016 à la faculté de Marketing et d''Agrosciences de Colmar.\r\nCette édition rend compte d''une collaboration originale entre un artiste et des scientifiques mais il est avant tout l''oeuvre de l''artiste.\r\nMartin Chramosta a promené sont regard lors de ses déambulations urbaines dans le patrimoine immobilier moyenâgeux alsacien. Il a extrait des petits éléments sculpturaux qu''il a repris à son compte et restitué sous un nouveau genre.\r\nLes têtes, figues et personnes étranges et fabuleux ont trouvé leur place dans son cabinet de curiosité réduit, ils content une histoire insolite à  la croisée d''un pratrimoine, d''une technique et d''une créativité.', '/assets/img/photos/oeuvres/Melsass.jpg', 'NULL', '1900-01-01 00:00:00'),
				(7, 'Modulation sur le carré ', 'Maxime Descombin', '1969', 5, '47.732126', '7.313828', 2, 'Maximum Descombin est autodidacte.\r\nAprès un apprentissage de tailleur de pierre, il débute en sculpture par l''ornementation dans le bâtiment et l''art funéraire, fréquente une école de dessin à Mâcon, travaille au service du sculpteur Désiré Mathivet au Villars, près de Tournus où il rencontre Antoine de Saint-Exupéry.\r\nDe ces années demeurent d''admirables portraits et des figures féminines empreintes d''une mélancolique gravité.\r\nA la Libération, la série des « oiseaux tués » hommage aux amis disparus se développe dans des formes souples et déjà abstraites. Après la guerre et dans les années cinquante, galeries et salons parisiens, revues artistiques de premier plan rendent compte de ses travaux.\r\nDescombin participe à de nombreuses expositions en France et à l''étranger (Allemagne, Etats-Unis, Japon …).\r\nIl participe à l''Interbau de Berlin en 1957, au Symposium international de sculpture de Grenoble en 1967.\r\nDescombin œuvre dans le cadre du 1% et de la commande publique.\r\nL''Association pour l''Atelier Descombin est fondée en 1991, sous la présidence d''honneur de Georges Duby. Un musée atelier est construit à Champlevert.\r\nIl décède le 28 août 2003.\r\n', '/assets/img/photos/oeuvres/Modulation_sur_le_carre.jpg', 'NULL', '1900-01-01 00:00:00'),
				(8, 'Résidence de Laure Ledoux à l''ENSISA', 'Laure Ledoux', '2014', 3, '47.730400', '7.310958', 2, 'à définir', '/assets/img/photos/oeuvres/Residence_de_Laure_Ledoux_ENSISA.jpg', 'NULL', '1900-01-01 00:00:00'),
				(9, 'ZOUTMA', 'Véronique Werner', '2008', 5, '47.732074', '7.313474', 2, 'Œuvre en collaboration avec Yves Carrey.\r\nVéronique Werner est une artiste mulhousienne.\r\n\r\nVéronique Werner réalise des fresques, des sculptures et des objets décoratifs, en assemblant des matériaux récupérés et en incrustant des fragments divers telles que verreries ou céramiques dans du cient. \r\nCette œuvre « ZOUTMA » est une commande de la Ville de Mulhouse dans le cadre des « sculptures estivales », une série de 4 trônes surdimensionnés. Le « ZOUTMA » s''inspire du fauteuil « club » par ses rondeurs et ses éléments assemblés : cuves à mazout et fûts.\r\nŒuvre mise à disposition sur le campus par la Ville de Mulhouse.', '/assets/img/photos/oeuvres/ZOUTMA.jpg', 'NULL', '1900-01-01 00:00:00');";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();

		$sql = "INSERT INTO `users_rights` (`id`, `rights`) VALUES
				(1, 'User'),
				(2, 'Admin');";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();

		$sql = "INSERT INTO `users` (`id`, `pseudo`, `password`, `firstname`, `lastname`, `email`, `rights`, `password_changed`, `date_creation`) VALUES
				(1, 'UHA', '$2y$10\$paJdLkoDOCAjzpoxbfY.f.rMOyO.FKyLeGdVSpnuMekQR276H0l9O', 'UHA', 'UHA', 'contact@uha.fr', 2, 1, '2018-11-11 10:35:27'),
				(2, 'nfourie', '$2y$10$3OnG4EV4BSbBAVzUEfBQWORj5WcD7rc4lK2cLMYZ3ogG8FtKwWMxe', 'Natan', 'FOURIÉ', 'natan.fourie@uha.fr', 2, 1, '2018-11-11 10:35:28'),
				(3, 'gstaehler', '$2y$10\$jryYio.yipO1/3ynkvXgy..3SRmPqmnDI/RGI/S0xKxxoP3HEShHi', 'Gauthier', 'STAEHLER', 'gauthier.staehler@uha.fr', 2, 1, '2018-11-11 10:35:29'),
				(4, 'cbourgeois', '$2y$10$88M4WcejqVbRgc7aBA5YhOAwtYKszKvgPWS3E/76g68tHMYMbp5P2', 'Christophe', 'BOURGEOIS', 'christophe.bourgeois@uha.fr', 2, 1, '2018-11-11 10:35:30'),
				(5, 'amartinez', '$2y$10\$dVz3iSFX7qJc/T8heH/f7.aswRArKGErgg0vYIb06oLMAfL4zIy9G', 'Alexis', 'MARTINEZ', 'alexis.martinez@uha.fr', 2, 1, '2018-11-11 10:35:31'),
				(6, 'aroyer', '$2y$10$1F2l67xzMfUWw585RsoDk.V5bMZtmSG8TNPY.k8pWbYsH6fJof/dK', 'Alexis', 'ROYER', 'alexis.royer@uha.fr', 2, 1, '2018-11-11 10:35:31'),
				(7, 'ikarapostolis', '$2y$10\$yZ4n4fzanPwvRXmdiomt2OdgwOFWx/uXLFPWTpwH8om/.s/5MYg0u', 'Ioannis', 'KARAPOSTOLIS', 'ioannis.karapostolis@uha.fr', 2, 1, '2018-11-11 10:35:31'),
				(8, 'sbrichler', '$2y$10\$L6G3/hYevxnPSwu533AFlO5feiCYYxH8rp9tCt0a5Ykv.pRib6g/2', 'Stephane', 'BRICHLER', 'stephane.brichler@uha.fr', 2, 1, '2018-11-11 10:35:31'),
				(9, 'fbourgeois', '$2y$10\$at5lnL3sZqCWIpEnKPz7w.0XAr13GmXxT2RQCvjpkoFlKpKI1tgwy', 'Florent', 'BOURGEOIS', 'florent.bourgeois@uha.fr', 2, 1, '2018-11-11 10:35:32'),
				(10, 'dda-fonseca', '$2y$10\$YviddTkqV/Z8wIyWqPeksOs//pY8le11KWT8F.Lj1eqpZ77vhj.D6', 'Daniel', 'DA-FONSECA', 'daniel.da-fonseca@uha.fr', 2, 1, '2018-11-11 10:35:33'),
				(11, 'vbreyer', '$2y$10\$M9YGkEl4hmIK33ryR8o2Eu3fJG1v5YIs3irIDu9Zy3cnZHUpaE402', 'Viviane', 'BREYER', 'viviane.breyer@uha.fr', 2, 1, '2019-02-14 15:35:33');";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  			'monuments_admin' => '
				# This is a fix for InnoDB in MySQL >= 4.1.x
				# It "suspends judgement" for fkey relationships until are tables are set.
				SET FOREIGN_KEY_CHECKS = 0;

				CREATE TABLE `campus_site`
				(
					`id` INTEGER NOT NULL AUTO_INCREMENT,
					`campus` VARCHAR(255) NOT NULL,
					`latitude` VARCHAR(255) NOT NULL,
					`longitude` VARCHAR(255) NOT NULL,
					PRIMARY KEY (`id`),
					UNIQUE INDEX `campus` (`campus`)
				) ENGINE=InnoDB;

				CREATE TABLE `category`
				(
					`id` INTEGER NOT NULL AUTO_INCREMENT,
					`categorie` VARCHAR(255) NOT NULL,
					`image` VARCHAR(255) NOT NULL,
					PRIMARY KEY (`id`),
					UNIQUE INDEX `categorie` (`categorie`)
				) ENGINE=InnoDB;

				CREATE TABLE `oeuvres`
				(
					`id` INTEGER NOT NULL AUTO_INCREMENT,
					`nom` VARCHAR(255) NOT NULL,
					`artiste` VARCHAR(255) NOT NULL,
					`annee` VARCHAR(255) NOT NULL,
					`categorie` INTEGER NOT NULL,
					`latitude` VARCHAR(255) NOT NULL,
					`longitude` VARCHAR(255) NOT NULL,
					`campus` INTEGER NOT NULL,
					`description` TEXT NOT NULL,
					`image` VARCHAR(255) NOT NULL,
					`droits_auteur` VARCHAR(255) NOT NULL,
					`date_creation` DATETIME NOT NULL,
					PRIMARY KEY (`id`),
					INDEX `categorie` (`categorie`),
					INDEX `campus` (`campus`),
					CONSTRAINT `campus`
						FOREIGN KEY (`campus`)
						REFERENCES `campus_site` (`id`),
					CONSTRAINT `categorie`
						FOREIGN KEY (`categorie`)
						REFERENCES `category` (`id`)
				) ENGINE=InnoDB;

				CREATE TABLE `users_rights`
				(
					`id` INTEGER NOT NULL AUTO_INCREMENT,
					`rights` VARCHAR(255) NOT NULL,
					PRIMARY KEY (`id`),
					UNIQUE INDEX `rights` (`rights`)
				) ENGINE=InnoDB;

				CREATE TABLE `users`
				(
					`id` INTEGER NOT NULL AUTO_INCREMENT,
					`pseudo` VARCHAR(255) NOT NULL,
					`password` VARCHAR(255) NOT NULL,
					`firstname` VARCHAR(255) NOT NULL,
					`lastname` VARCHAR(255) NOT NULL,
					`email` VARCHAR(255) NOT NULL,
					`rights` INTEGER NOT NULL,
					`password_changed` INTEGER NOT NULL,
					`date_creation` DATETIME NOT NULL,
					PRIMARY KEY (`id`),
					INDEX `rights` (`rights`),
					CONSTRAINT `rights`
						FOREIGN KEY (`rights`)
						REFERENCES `users_rights` (`id`)
				) ENGINE=InnoDB;

				# This restores the fkey checks, after having unset them earlier
				SET FOREIGN_KEY_CHECKS = 1;
			',
		);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
	public function getDownSQL()
	{
        return array (
  			'monuments_admin' => '
				# This is a fix for InnoDB in MySQL >= 4.1.x
				# It "suspends judgement" for fkey relationships until are tables are set.
				SET FOREIGN_KEY_CHECKS = 0;

				DROP TABLE IF EXISTS `campus_site`;

				DROP TABLE IF EXISTS `category`;

				DROP TABLE IF EXISTS `oeuvres`;

				DROP TABLE IF EXISTS `users`;

				DROP TABLE IF EXISTS `users_rights`;

				# This restores the fkey checks, after having unset them earlier
				SET FOREIGN_KEY_CHECKS = 1;
			',
		);
    }
}
