@echo off
REM || Defin window title.
TITLE  Gource (git vizualiser) - Monuments

REM || Define const & parameters.
set title=Monuments-git-vizualiser
set fullScreen=-f
set screenRes=-1280x720
set outputFramerate=60
set autoSkipSeconds=1
set secondsPerDay=2
set maxFiles=0
set maxFileLag=0.5
set cameraMode=overview
set crop=vertical
set padding=1.5
set captionFile=gource.caption
set outputPpmStream=gource.ppm
set configFile=gource.conf
set logCommand=git
set logFormat=git

REM || Start Gource with parameters.
REM gource --title %title% %screenRes% --output-framerate %outputFramerate% --disable-bloom --auto-skip-seconds %autoSkipSeconds% --seconds-per-day %secondsPerDay% --stop-at-end --key --max-files %maxFiles% --max-file-lag %maxFileLag% --camera-mode %cameraMode% --crop %crop% --padding %padding% --caption-file %captionFile% --output-ppm-stream %outputPpmStream% --save-config %configFile%
gource --load-config %configFile%
REM gource --save-config %configFile%
REM --log-command %logCommand% --log-format %logFormat%
PAUSE