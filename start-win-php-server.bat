@echo off
set localhost=127.0.0.1
set external_bind=0.0.0.0
set port=8000

REM Open php web-server in web explorer on localhost:8000.
explorer http://localhost:%port%

REM Start php web server on localhost on port 8000.
php -S %localhost%:%port%

REM Start php web server on all interfaces on port 8000.
::php -S %external_bind%:%port%