<?php

namespace Base;

use \Oeuvres as ChildOeuvres;
use \OeuvresQuery as ChildOeuvresQuery;
use \Exception;
use \PDO;
use Map\OeuvresTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'oeuvres' table.
 *
 *
 *
 * @method     ChildOeuvresQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOeuvresQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildOeuvresQuery orderByArtiste($order = Criteria::ASC) Order by the artiste column
 * @method     ChildOeuvresQuery orderByAnnee($order = Criteria::ASC) Order by the annee column
 * @method     ChildOeuvresQuery orderByCategorie($order = Criteria::ASC) Order by the categorie column
 * @method     ChildOeuvresQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildOeuvresQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 * @method     ChildOeuvresQuery orderByCampus($order = Criteria::ASC) Order by the campus column
 * @method     ChildOeuvresQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildOeuvresQuery orderByImage($order = Criteria::ASC) Order by the image column
 * @method     ChildOeuvresQuery orderBydroitsAuteur($order = Criteria::ASC) Order by the droits_auteur column
 * @method     ChildOeuvresQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 *
 * @method     ChildOeuvresQuery groupById() Group by the id column
 * @method     ChildOeuvresQuery groupByNom() Group by the nom column
 * @method     ChildOeuvresQuery groupByArtiste() Group by the artiste column
 * @method     ChildOeuvresQuery groupByAnnee() Group by the annee column
 * @method     ChildOeuvresQuery groupByCategorie() Group by the categorie column
 * @method     ChildOeuvresQuery groupByLatitude() Group by the latitude column
 * @method     ChildOeuvresQuery groupByLongitude() Group by the longitude column
 * @method     ChildOeuvresQuery groupByCampus() Group by the campus column
 * @method     ChildOeuvresQuery groupByDescription() Group by the description column
 * @method     ChildOeuvresQuery groupByImage() Group by the image column
 * @method     ChildOeuvresQuery groupBydroitsAuteur() Group by the droits_auteur column
 * @method     ChildOeuvresQuery groupByDateCreation() Group by the date_creation column
 *
 * @method     ChildOeuvresQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOeuvresQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOeuvresQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOeuvresQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOeuvresQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOeuvresQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOeuvresQuery leftJoinCampusSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the CampusSite relation
 * @method     ChildOeuvresQuery rightJoinCampusSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CampusSite relation
 * @method     ChildOeuvresQuery innerJoinCampusSite($relationAlias = null) Adds a INNER JOIN clause to the query using the CampusSite relation
 *
 * @method     ChildOeuvresQuery joinWithCampusSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CampusSite relation
 *
 * @method     ChildOeuvresQuery leftJoinWithCampusSite() Adds a LEFT JOIN clause and with to the query using the CampusSite relation
 * @method     ChildOeuvresQuery rightJoinWithCampusSite() Adds a RIGHT JOIN clause and with to the query using the CampusSite relation
 * @method     ChildOeuvresQuery innerJoinWithCampusSite() Adds a INNER JOIN clause and with to the query using the CampusSite relation
 *
 * @method     ChildOeuvresQuery leftJoinCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the Category relation
 * @method     ChildOeuvresQuery rightJoinCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Category relation
 * @method     ChildOeuvresQuery innerJoinCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the Category relation
 *
 * @method     ChildOeuvresQuery joinWithCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Category relation
 *
 * @method     ChildOeuvresQuery leftJoinWithCategory() Adds a LEFT JOIN clause and with to the query using the Category relation
 * @method     ChildOeuvresQuery rightJoinWithCategory() Adds a RIGHT JOIN clause and with to the query using the Category relation
 * @method     ChildOeuvresQuery innerJoinWithCategory() Adds a INNER JOIN clause and with to the query using the Category relation
 *
 * @method     \CampusSiteQuery|\CategoryQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOeuvres findOne(ConnectionInterface $con = null) Return the first ChildOeuvres matching the query
 * @method     ChildOeuvres findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOeuvres matching the query, or a new ChildOeuvres object populated from the query conditions when no match is found
 *
 * @method     ChildOeuvres findOneById(int $id) Return the first ChildOeuvres filtered by the id column
 * @method     ChildOeuvres findOneByNom(string $nom) Return the first ChildOeuvres filtered by the nom column
 * @method     ChildOeuvres findOneByArtiste(string $artiste) Return the first ChildOeuvres filtered by the artiste column
 * @method     ChildOeuvres findOneByAnnee(string $annee) Return the first ChildOeuvres filtered by the annee column
 * @method     ChildOeuvres findOneByCategorie(int $categorie) Return the first ChildOeuvres filtered by the categorie column
 * @method     ChildOeuvres findOneByLatitude(string $latitude) Return the first ChildOeuvres filtered by the latitude column
 * @method     ChildOeuvres findOneByLongitude(string $longitude) Return the first ChildOeuvres filtered by the longitude column
 * @method     ChildOeuvres findOneByCampus(int $campus) Return the first ChildOeuvres filtered by the campus column
 * @method     ChildOeuvres findOneByDescription(string $description) Return the first ChildOeuvres filtered by the description column
 * @method     ChildOeuvres findOneByImage(string $image) Return the first ChildOeuvres filtered by the image column
 * @method     ChildOeuvres findOneBydroitsAuteur(string $droits_auteur) Return the first ChildOeuvres filtered by the droits_auteur column
 * @method     ChildOeuvres findOneByDateCreation(string $date_creation) Return the first ChildOeuvres filtered by the date_creation column *

 * @method     ChildOeuvres requirePk($key, ConnectionInterface $con = null) Return the ChildOeuvres by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOne(ConnectionInterface $con = null) Return the first ChildOeuvres matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOeuvres requireOneById(int $id) Return the first ChildOeuvres filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByNom(string $nom) Return the first ChildOeuvres filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByArtiste(string $artiste) Return the first ChildOeuvres filtered by the artiste column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByAnnee(string $annee) Return the first ChildOeuvres filtered by the annee column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByCategorie(int $categorie) Return the first ChildOeuvres filtered by the categorie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByLatitude(string $latitude) Return the first ChildOeuvres filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByLongitude(string $longitude) Return the first ChildOeuvres filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByCampus(int $campus) Return the first ChildOeuvres filtered by the campus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByDescription(string $description) Return the first ChildOeuvres filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByImage(string $image) Return the first ChildOeuvres filtered by the image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneBydroitsAuteur(string $droits_auteur) Return the first ChildOeuvres filtered by the droits_auteur column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOeuvres requireOneByDateCreation(string $date_creation) Return the first ChildOeuvres filtered by the date_creation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOeuvres[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOeuvres objects based on current ModelCriteria
 * @method     ChildOeuvres[]|ObjectCollection findById(int $id) Return ChildOeuvres objects filtered by the id column
 * @method     ChildOeuvres[]|ObjectCollection findByNom(string $nom) Return ChildOeuvres objects filtered by the nom column
 * @method     ChildOeuvres[]|ObjectCollection findByArtiste(string $artiste) Return ChildOeuvres objects filtered by the artiste column
 * @method     ChildOeuvres[]|ObjectCollection findByAnnee(string $annee) Return ChildOeuvres objects filtered by the annee column
 * @method     ChildOeuvres[]|ObjectCollection findByCategorie(int $categorie) Return ChildOeuvres objects filtered by the categorie column
 * @method     ChildOeuvres[]|ObjectCollection findByLatitude(string $latitude) Return ChildOeuvres objects filtered by the latitude column
 * @method     ChildOeuvres[]|ObjectCollection findByLongitude(string $longitude) Return ChildOeuvres objects filtered by the longitude column
 * @method     ChildOeuvres[]|ObjectCollection findByCampus(int $campus) Return ChildOeuvres objects filtered by the campus column
 * @method     ChildOeuvres[]|ObjectCollection findByDescription(string $description) Return ChildOeuvres objects filtered by the description column
 * @method     ChildOeuvres[]|ObjectCollection findByImage(string $image) Return ChildOeuvres objects filtered by the image column
 * @method     ChildOeuvres[]|ObjectCollection findBydroitsAuteur(string $droits_auteur) Return ChildOeuvres objects filtered by the droits_auteur column
 * @method     ChildOeuvres[]|ObjectCollection findByDateCreation(string $date_creation) Return ChildOeuvres objects filtered by the date_creation column
 * @method     ChildOeuvres[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OeuvresQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\OeuvresQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'monuments_admin', $modelName = '\\Oeuvres', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOeuvresQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOeuvresQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOeuvresQuery) {
            return $criteria;
        }
        $query = new ChildOeuvresQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOeuvres|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OeuvresTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OeuvresTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOeuvres A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nom, artiste, annee, categorie, latitude, longitude, campus, description, image, droits_auteur, date_creation FROM oeuvres WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOeuvres $obj */
            $obj = new ChildOeuvres();
            $obj->hydrate($row);
            OeuvresTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOeuvres|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OeuvresTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OeuvresTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the artiste column
     *
     * Example usage:
     * <code>
     * $query->filterByArtiste('fooValue');   // WHERE artiste = 'fooValue'
     * $query->filterByArtiste('%fooValue%', Criteria::LIKE); // WHERE artiste LIKE '%fooValue%'
     * </code>
     *
     * @param     string $artiste The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByArtiste($artiste = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($artiste)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_ARTISTE, $artiste, $comparison);
    }

    /**
     * Filter the query on the annee column
     *
     * Example usage:
     * <code>
     * $query->filterByAnnee('fooValue');   // WHERE annee = 'fooValue'
     * $query->filterByAnnee('%fooValue%', Criteria::LIKE); // WHERE annee LIKE '%fooValue%'
     * </code>
     *
     * @param     string $annee The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByAnnee($annee = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($annee)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_ANNEE, $annee, $comparison);
    }

    /**
     * Filter the query on the categorie column
     *
     * Example usage:
     * <code>
     * $query->filterByCategorie(1234); // WHERE categorie = 1234
     * $query->filterByCategorie(array(12, 34)); // WHERE categorie IN (12, 34)
     * $query->filterByCategorie(array('min' => 12)); // WHERE categorie > 12
     * </code>
     *
     * @see       filterByCategory()
     *
     * @param     mixed $categorie The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByCategorie($categorie = null, $comparison = null)
    {
        if (is_array($categorie)) {
            $useMinMax = false;
            if (isset($categorie['min'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_CATEGORIE, $categorie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categorie['max'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_CATEGORIE, $categorie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_CATEGORIE, $categorie, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the campus column
     *
     * Example usage:
     * <code>
     * $query->filterByCampus(1234); // WHERE campus = 1234
     * $query->filterByCampus(array(12, 34)); // WHERE campus IN (12, 34)
     * $query->filterByCampus(array('min' => 12)); // WHERE campus > 12
     * </code>
     *
     * @see       filterByCampusSite()
     *
     * @param     mixed $campus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByCampus($campus = null, $comparison = null)
    {
        if (is_array($campus)) {
            $useMinMax = false;
            if (isset($campus['min'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_CAMPUS, $campus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($campus['max'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_CAMPUS, $campus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_CAMPUS, $campus, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the image column
     *
     * Example usage:
     * <code>
     * $query->filterByImage('fooValue');   // WHERE image = 'fooValue'
     * $query->filterByImage('%fooValue%', Criteria::LIKE); // WHERE image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $image The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByImage($image = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($image)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_IMAGE, $image, $comparison);
    }

    /**
     * Filter the query on the droits_auteur column
     *
     * Example usage:
     * <code>
     * $query->filterBydroitsAuteur('fooValue');   // WHERE droits_auteur = 'fooValue'
     * $query->filterBydroitsAuteur('%fooValue%', Criteria::LIKE); // WHERE droits_auteur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $droitsAuteur The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterBydroitsAuteur($droitsAuteur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($droitsAuteur)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_DROITS_AUTEUR, $droitsAuteur, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(OeuvresTableMap::COL_DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OeuvresTableMap::COL_DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query by a related \CampusSite object
     *
     * @param \CampusSite|ObjectCollection $campusSite The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByCampusSite($campusSite, $comparison = null)
    {
        if ($campusSite instanceof \CampusSite) {
            return $this
                ->addUsingAlias(OeuvresTableMap::COL_CAMPUS, $campusSite->getId(), $comparison);
        } elseif ($campusSite instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OeuvresTableMap::COL_CAMPUS, $campusSite->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCampusSite() only accepts arguments of type \CampusSite or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CampusSite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function joinCampusSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CampusSite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CampusSite');
        }

        return $this;
    }

    /**
     * Use the CampusSite relation CampusSite object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CampusSiteQuery A secondary query class using the current class as primary query
     */
    public function useCampusSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCampusSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CampusSite', '\CampusSiteQuery');
    }

    /**
     * Filter the query by a related \Category object
     *
     * @param \Category|ObjectCollection $category The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOeuvresQuery The current query, for fluid interface
     */
    public function filterByCategory($category, $comparison = null)
    {
        if ($category instanceof \Category) {
            return $this
                ->addUsingAlias(OeuvresTableMap::COL_CATEGORIE, $category->getId(), $comparison);
        } elseif ($category instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OeuvresTableMap::COL_CATEGORIE, $category->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCategory() only accepts arguments of type \Category or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Category relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function joinCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Category');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Category');
        }

        return $this;
    }

    /**
     * Use the Category relation Category object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CategoryQuery A secondary query class using the current class as primary query
     */
    public function useCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Category', '\CategoryQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOeuvres $oeuvres Object to remove from the list of results
     *
     * @return $this|ChildOeuvresQuery The current query, for fluid interface
     */
    public function prune($oeuvres = null)
    {
        if ($oeuvres) {
            $this->addUsingAlias(OeuvresTableMap::COL_ID, $oeuvres->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the oeuvres table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OeuvresTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OeuvresTableMap::clearInstancePool();
            OeuvresTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OeuvresTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OeuvresTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OeuvresTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OeuvresTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // OeuvresQuery
