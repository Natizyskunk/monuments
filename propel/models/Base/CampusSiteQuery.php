<?php

namespace Base;

use \CampusSite as ChildCampusSite;
use \CampusSiteQuery as ChildCampusSiteQuery;
use \Exception;
use \PDO;
use Map\CampusSiteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'campus_site' table.
 *
 *
 *
 * @method     ChildCampusSiteQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCampusSiteQuery orderByCampus($order = Criteria::ASC) Order by the campus column
 * @method     ChildCampusSiteQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildCampusSiteQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 *
 * @method     ChildCampusSiteQuery groupById() Group by the id column
 * @method     ChildCampusSiteQuery groupByCampus() Group by the campus column
 * @method     ChildCampusSiteQuery groupByLatitude() Group by the latitude column
 * @method     ChildCampusSiteQuery groupByLongitude() Group by the longitude column
 *
 * @method     ChildCampusSiteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCampusSiteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCampusSiteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCampusSiteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCampusSiteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCampusSiteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCampusSiteQuery leftJoinOeuvres($relationAlias = null) Adds a LEFT JOIN clause to the query using the Oeuvres relation
 * @method     ChildCampusSiteQuery rightJoinOeuvres($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Oeuvres relation
 * @method     ChildCampusSiteQuery innerJoinOeuvres($relationAlias = null) Adds a INNER JOIN clause to the query using the Oeuvres relation
 *
 * @method     ChildCampusSiteQuery joinWithOeuvres($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Oeuvres relation
 *
 * @method     ChildCampusSiteQuery leftJoinWithOeuvres() Adds a LEFT JOIN clause and with to the query using the Oeuvres relation
 * @method     ChildCampusSiteQuery rightJoinWithOeuvres() Adds a RIGHT JOIN clause and with to the query using the Oeuvres relation
 * @method     ChildCampusSiteQuery innerJoinWithOeuvres() Adds a INNER JOIN clause and with to the query using the Oeuvres relation
 *
 * @method     \OeuvresQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCampusSite findOne(ConnectionInterface $con = null) Return the first ChildCampusSite matching the query
 * @method     ChildCampusSite findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCampusSite matching the query, or a new ChildCampusSite object populated from the query conditions when no match is found
 *
 * @method     ChildCampusSite findOneById(int $id) Return the first ChildCampusSite filtered by the id column
 * @method     ChildCampusSite findOneByCampus(string $campus) Return the first ChildCampusSite filtered by the campus column
 * @method     ChildCampusSite findOneByLatitude(string $latitude) Return the first ChildCampusSite filtered by the latitude column
 * @method     ChildCampusSite findOneByLongitude(string $longitude) Return the first ChildCampusSite filtered by the longitude column *

 * @method     ChildCampusSite requirePk($key, ConnectionInterface $con = null) Return the ChildCampusSite by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCampusSite requireOne(ConnectionInterface $con = null) Return the first ChildCampusSite matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCampusSite requireOneById(int $id) Return the first ChildCampusSite filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCampusSite requireOneByCampus(string $campus) Return the first ChildCampusSite filtered by the campus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCampusSite requireOneByLatitude(string $latitude) Return the first ChildCampusSite filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCampusSite requireOneByLongitude(string $longitude) Return the first ChildCampusSite filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCampusSite[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCampusSite objects based on current ModelCriteria
 * @method     ChildCampusSite[]|ObjectCollection findById(int $id) Return ChildCampusSite objects filtered by the id column
 * @method     ChildCampusSite[]|ObjectCollection findByCampus(string $campus) Return ChildCampusSite objects filtered by the campus column
 * @method     ChildCampusSite[]|ObjectCollection findByLatitude(string $latitude) Return ChildCampusSite objects filtered by the latitude column
 * @method     ChildCampusSite[]|ObjectCollection findByLongitude(string $longitude) Return ChildCampusSite objects filtered by the longitude column
 * @method     ChildCampusSite[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CampusSiteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CampusSiteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'monuments_admin', $modelName = '\\CampusSite', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCampusSiteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCampusSiteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCampusSiteQuery) {
            return $criteria;
        }
        $query = new ChildCampusSiteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCampusSite|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CampusSiteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CampusSiteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCampusSite A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, campus, latitude, longitude FROM campus_site WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCampusSite $obj */
            $obj = new ChildCampusSite();
            $obj->hydrate($row);
            CampusSiteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCampusSite|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CampusSiteTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CampusSiteTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CampusSiteTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CampusSiteTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CampusSiteTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the campus column
     *
     * Example usage:
     * <code>
     * $query->filterByCampus('fooValue');   // WHERE campus = 'fooValue'
     * $query->filterByCampus('%fooValue%', Criteria::LIKE); // WHERE campus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $campus The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterByCampus($campus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($campus)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CampusSiteTableMap::COL_CAMPUS, $campus, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CampusSiteTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CampusSiteTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query by a related \Oeuvres object
     *
     * @param \Oeuvres|ObjectCollection $oeuvres the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCampusSiteQuery The current query, for fluid interface
     */
    public function filterByOeuvres($oeuvres, $comparison = null)
    {
        if ($oeuvres instanceof \Oeuvres) {
            return $this
                ->addUsingAlias(CampusSiteTableMap::COL_ID, $oeuvres->getCampus(), $comparison);
        } elseif ($oeuvres instanceof ObjectCollection) {
            return $this
                ->useOeuvresQuery()
                ->filterByPrimaryKeys($oeuvres->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOeuvres() only accepts arguments of type \Oeuvres or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Oeuvres relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function joinOeuvres($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Oeuvres');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Oeuvres');
        }

        return $this;
    }

    /**
     * Use the Oeuvres relation Oeuvres object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \OeuvresQuery A secondary query class using the current class as primary query
     */
    public function useOeuvresQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOeuvres($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Oeuvres', '\OeuvresQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCampusSite $campusSite Object to remove from the list of results
     *
     * @return $this|ChildCampusSiteQuery The current query, for fluid interface
     */
    public function prune($campusSite = null)
    {
        if ($campusSite) {
            $this->addUsingAlias(CampusSiteTableMap::COL_ID, $campusSite->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the campus_site table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CampusSiteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CampusSiteTableMap::clearInstancePool();
            CampusSiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CampusSiteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CampusSiteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CampusSiteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CampusSiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CampusSiteQuery
