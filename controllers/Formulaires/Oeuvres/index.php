<?php
$regYear 	 = "#^[0-9]{1,4}$#";
$regLat 	 = "#^-?((90)|([1-8]?[0-9](\.[0-9]+)?))$#";
$regLng 	 = "#^-?((180)|([1]?[0-7]?[0-9](\.[0-9]+)?))$#";
$date 		 = date('Y-m-d H:i:s', time());
$maxFileSize = 5000000; // 5Mo.

if (isset($_POST) && !empty($_POST)) {
	if (isset($_POST['submit'])) {
		if (!empty($_POST['nameOeuvre']) && !empty($_POST['artisteName']) && !empty($_POST['oeuvreAnnee']) && !empty($_POST['oeuvreTextarea']) && !empty($_POST['latitude']) && !empty($_POST['longitude']) && !empty($_POST['oeuvreCatego']) && !empty($_POST['oeuvreCampus']) && !empty($_POST['droitsAuteur']) && !empty($_FILES['fileUser'])) {
			if (preg_match($regYear, $_POST['oeuvreAnnee']) && preg_match($regLat, $_POST['latitude']) && preg_match($regLng, $_POST['longitude'])) {
				if ($_FILES['fileUser']['error'] == 0) {
					if ($_FILES['fileUser']['size'] > $maxFileSize) {
						$aMessageError[] = 'L\'image ne doit pas dépasser 5Mo.';
					}
					else {
						$infosfichier           = pathinfo($_FILES['fileUser']['name']);
						$typeMime               = mime_content_type($_FILES['fileUser']['tmp_name']);
						$extension_upload       = $infosfichier['extension'];
						$target_dir             = _ASSETS_."img/photos/oeuvres/";
						$extensions_autorisees  = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'tiff', 'webp');
						$autorizedMimeTypes     = array(
							"image/bmp",
							"image/gif",
							"image/jpg",
							"image/jpeg",
							"image/png",
							"image/tiff",
							"image/webp"
						);

						if (!in_array($extension_upload, $extensions_autorisees)) {
							$aMessageError[] = "L'extension '".$extension_upload."' n'est pas autorisée. Merci de choisir une image avec comme extension 'bmp, gif, jpg, jpeg, png, tiff, webp'.";
						}
						else {
							if (!in_array($typeMime, $autorizedMimeTypes)) {
								$aMessageError[] = "Le contenu de fichier '".$typeMime."' n'est pas autorisé. Attention Ceci n'est pas une image !";
							}
							else {
								move_uploaded_file($_FILES['fileUser']['tmp_name'], $target_dir.$_FILES['fileUser']['name']);
								$name 	  	 = htmlspecialchars($_POST['nameOeuvre']);
								$artiste 	 = htmlspecialchars($_POST['artisteName']);
								$annee 		 = htmlspecialchars($_POST['oeuvreAnnee']);
								$Description = htmlspecialchars($_POST['oeuvreTextarea']);
								$latitude  	 = htmlspecialchars($_POST['latitude']);
								$longitude 	 = htmlspecialchars($_POST['longitude']);
								$categorie 	 = htmlspecialchars($_POST['oeuvreCatego']);
								$campus 	 = htmlspecialchars($_POST['oeuvreCampus']);
								$droitsAuteur = htmlspecialchars($_POST['droitsAuteur']);

								$oeuvre = new Oeuvres();
								$oeuvre->setNom($name);
								$oeuvre->setArtiste($artiste);
								$oeuvre->setAnnee( $annee);
								$oeuvre->setCategorie($categorie);
								$oeuvre->setLatitude($latitude);
								$oeuvre->setLongitude($longitude);
								$oeuvre->setCampus($campus);
								$oeuvre->setDescription($Description);
								$oeuvre->setImage('/assets/img/photos/oeuvres/'. $_FILES['fileUser']['name']);
								$oeuvre->setDroitsAuteur($droitsAuteur);
								$oeuvre->setDateCreation($date);
								$oeuvre->save();

								$_SESSION['NewOeuvreMessageSuccess'] 	= "La nouvelle oeuvre intitulée '".$name."' à bien été ajoutée.";
								header('Location: /patrimoine');
							}
						}
					}
				}
				else {
					$aMessageError[] = 'Erreur lors du transfert.';
				}
			}
			else {
				$aMessageError[] = 'Les coordonnées sont inconnues. Merci d\'en renseigner des existantes.';
			}
		}
		else {
			$aMessageError[] = 'Le formulaire doit être complet pour être soumis.';
		}
	}
}

$CategoryQuery 	= new CategoryQuery();
$categories 	= $CategoryQuery->find();

$aCategoriesInformations = array();

foreach($categories as $categorie) {
    $aCategorie = array(
        'id' 		=> $categorie->getId(),
		'categorie'	=> $categorie->getCategorie()
    );
    $aCategoriesInformations[] = $aCategorie;
}

$campus 	= new CampusSiteQuery();
$campuses 	= $campus->find();

$aCampusInformations = array();

foreach($campuses as $campus) {
    $aCampus = array(
        'id' 		=> $campus->getId(),
		'campus' 	=> $campus->getCampus()
    );
    $aCampusesInformations[] = $aCampus;
}

$smarty->assign('aCategoriesInformations', $aCategoriesInformations);
$smarty->assign('aCampusesInformations', $aCampusesInformations);
$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Formulaires/Oeuvres/index.html');
?>
