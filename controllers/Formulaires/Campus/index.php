<?php
if (isset($_SESSION['messageSuccessCampusSuppr'])) {
	$aMessageSuccess[] = $_SESSION['messageSuccessCampusSuppr'];
	unset($_SESSION['messageSuccessCampusSuppr']);
}

if (isset($_SESSION['messageSuccessCampusMaj'])) {
	$aMessageSuccess[] = $_SESSION['messageSuccessCampusMaj'];
	unset($_SESSION['messageSuccessCampusMaj']);
}

$campusSiteQuery = new CampusSiteQuery();

$regLat = "#^-?((90)|([1-8]?[0-9](\.[0-9]+)?))$#";
$regLng = "#^-?((180)|([1]?[0-7]?[0-9](\.[0-9]+)?))$#";

// Ajout d'un enregistrement Campus avec ses coordonnées
if (isset($_POST) && !empty($_POST)) {
	if (!empty($_POST['nameCampus']) && !empty($_POST['latitude']) && !empty($_POST['longitude'] )) {
		if (preg_match($regLat, $_POST['latitude']) && preg_match($regLng, $_POST['longitude'])) {
			$setName = htmlspecialchars($_POST['nameCampus']);
			$setLatitude = htmlspecialchars($_POST['latitude']);
			$setLongitude = htmlspecialchars($_POST['longitude']);

			$campus = new CampusSite();
			$campus->setCampus($setName);
			$campus->setLatitude($setLatitude);
			$campus->setLongitude($setLongitude);
			$campus->save();

			$aMessageSuccess[] = 'Le nouveau Campus '.$setName.' est enregistré';
		}
		else {
			$aMessageError[] = 'format coordonnées ou année incorrect';
		}
	}
	else {
		$aMessageError[] = 'Le formulaire doit être complet pour être soumis.';
	}

	// Supprime un Campus
	if (!empty($_POST['idCampus']) && isset($_POST['supprButton'])) {
		$idCampusValue 	= $_POST['idCampus'];
		$campus 		= $campusSiteQuery->findOneById($idCampusValue);
		$campus->delete();

		$_SESSION['messageSuccessCampusSuppr'] = 'Le Campus est supprimé';
		header('Location: /form-campus');
		//$smarty->assign('idCampusValue', $idCampusValue);
	}

	// MAJ du Campus
	if (!empty($_POST['idCampus']) && isset($_POST['validationButton']) && !empty($_POST['nameCampusInput']) && !empty($_POST['latitudeInput']) && !empty($_POST['longitudeInput'])){
		$idCampusValue 			= $_POST['idCampus'];
		$nameCampusInputValue 	= htmlspecialchars($_POST['nameCampusInput']);
		$latitudeInputValue 	= htmlspecialchars($_POST['latitudeInput']);
		$longitudeInputValue 	= htmlspecialchars($_POST['longitudeInput']);

		$campus = $campusSiteQuery->findOneById($idCampusValue);
		$campus->setCampus($nameCampusInputValue);
		$campus->setLatitude($latitudeInputValue);
		$campus->setLongitude($longitudeInputValue);
		//$author->setLastName('Austen');
		$campus->save();

		$_SESSION['messageSuccessCampusMaj'] = 'Le Campus '.$nameCampusInputValue.' a été mis à jour';
		header('Location: /form-campus');
		//$smarty->assign('idCampusValue', $idCampusValue);
	}
}

// Remplit le tableau de la liste des Campus
$aCampusInformations = array();

// Pour recherche d'un campus
$campusFind  = $campusSiteQuery->find();

foreach ($campusFind as $campus) {
	$aCampus = array(
		'id' 		=> $campus->getId(),
		'campus' 	=> $campus->getCampus(),
		'latitude' 	=> $campus->getLatitude(),
		'longitude' => $campus->getLongitude(),
	);
	$aCampusInformations[] = $aCampus;
}

$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->assign('aCampusInformations', $aCampusInformations);
$smarty->display(_TPL_ . 'Formulaires/Campus/index.html');
?>