<?php
if (isset($_SESSION['messageSuccessCategoryAdd'])) {
    $aMessageSuccess[] = $_SESSION['messageSuccessCategoryAdd'];
    unset($_SESSION['messageSuccessCategoryAdd']);
}
if (isset($_SESSION['messageSuccessCategorieDeleted'])) {
    $aMessageSuccess[] = $_SESSION['messageSuccessCategorieDeleted'];
    unset($_SESSION['messageSuccessCategorieDeleted']);
}
if (isset($_SESSION['messageSuccessCategorieUpDated'])) {
    $aMessageSuccess[] = $_SESSION['messageSuccessCategorieUpDated'];
    unset($_SESSION['messageSuccessCategorieUpDated']);
}

$maxFileSize    = 1500000; // 1.5Mo.
$imageMaxWidth  = 64; //64px
$imageMaxHeight = 64; //64px
$categoryQuery  = new CategoryQuery();
$categories     = $categoryQuery->find();

foreach($categories as $categorie) {
    $aCategory = array(
        'id'        => $categorie->getId(),
        'categorie' => $categorie->getCategorie(),
        'image'     => $categorie->getImage()
    );
    $categoryInformations[] = $aCategory;
}


if (isset($_POST) && !empty($_POST)) {
    // Pour ajouter une catégorie.
    if (isset($_POST['nameCategory'])) {
		if (!empty($_POST['nameCategory']) && !empty($_FILES['fileIcon']['name'])) {
	        if ($_FILES['fileIcon']['error'] == 0) {
                if ($_FILES['fileIcon']['size'] > $maxFileSize) {
                    $aMessageError[] = "L'image ne doit pas dépasser 1.5Mo.";
				}
				else {
                    $infosfichier           = pathinfo($_FILES['fileIcon']['name']);
                    $typeMime               = mime_content_type($_FILES['fileIcon']['tmp_name']);
                    $extension_upload       = $infosfichier['extension'];
                    $target_dir             = _ASSETS_ . "img/icons/map/category/";
                    $extensions_autorisees  = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'tiff', 'webp');
                    $autorizedMimeTypes     = array(
                        "image/bmp",
                        "image/gif",
                        "image/jpg",
                        "image/jpeg",
                        "image/png",
                        "image/tiff",
                        "image/webp"
                    );

                    if (!in_array($extension_upload, $extensions_autorisees)) {
                        $aMessageError[] = "L'extension '".$extension_upload."' n'est pas autorisée. Merci de choisir une image avec comme extension 'bmp, gif, jpg, jpeg, png, tiff, webp'.";
                    }
                    else {
                        if (!in_array($typeMime, $autorizedMimeTypes)) {
                            $aMessageError[] = "Le contenu de fichier '".$typeMime."' n'est pas autorisé. Attention Ceci n'est pas une image !";
                        }
                        else {
                            $image_sizes = getimagesize($_FILES['fileIcon']['name']);

                            if ($image_sizes[0] > $imageMaxWidth OR $image_sizes[1] > $imageMaxHeight) {
                                $aMessageError[] = "Taille d'image trop grande. Dimension maximum acceptée: '64px*64px'.";
                            }
                            elseif ($image_sizes[0] <= $imageMaxWidth && $image_sizes[1] <= $imageMaxHeight) {
                                move_uploaded_file($_FILES['fileIcon']['tmp_name'], $target_dir.$_FILES['fileIcon']['name']);
                                $categoryName   = htmlspecialchars($_POST['nameCategory']);

                                $catgorie = new Category();
                                $catgorie->setCategorie($categoryName);
                                $catgorie->setImage('/assets/img/icons/map/category/'.$_FILES['fileIcon']['name']);
                                $catgorie->save();
                                $_SESSION['messageSuccessCategoryAdd'] = "La nouvelle catégorie '" . $categoryName . "' à bien été ajoutée.";
                                header('Location: /form-category');
                            }
                        }
                    }
                }
			}
			else {
                $aMessageError[] = 'Erreur lors du transfert.';
            }
		}
		else {
            $aMessageError[] = 'Le formulaire doit être complet pour être soumis.';
        }
    }


    // Pour supprimer une categorie.
    if (isset($_POST['supprButton'])) {
        $maCategorieId  = htmlspecialchars($_POST['categoryId']);
        $categorie      = $categoryQuery->findOneById($maCategorieId);
        $maCategorie    = $categorie->getCategorie();
        $monImage       = $categorie->getImage();
        $categorie->delete();
        unlink(_PATH_.'/'.$monImage);
        $_SESSION['messageSuccessCategorieDeleted'] = "La catégorie '".$maCategorie."' a été supprimée.";
        header('Location: /form-category');
    }


    // Pour éditer une catégorie.
    if (isset($_POST['validationButton'])) {
        if (isset($_POST['categoryRow']) && !empty($_POST['categoryRow'])){
            if (!empty($_FILES['imgRow']['name'])) {
                if ($_FILES['imgRow']['error'] == 0) {
                    if ($_FILES['imgRow']['size'] > $maxFileSize) {
                        $aMessageError[] = 'L\'image ne doit pas dépasser 1.5Mo.';
                    }
                    else {
                        $infosfichier           = pathinfo($_FILES['imgRow']['name']);
                        $typeMime               = mime_content_type($_FILES['imgRow']['tmp_name']);
                        $extension_upload       = $infosfichier['extension'];
                        $target_dir             = _ASSETS_ . "img/icons/map/category/";
                        $extensions_autorisees  = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'tiff', 'webp');
                        $autorizedMimeTypes     = array(
                            "image/bmp",
                            "image/gif",
                            "image/jpeg",
                            "image/png",
                            "image/tiff",
                            "image/webp"
                        );

                        if (!in_array($extension_upload, $extensions_autorisees)) {
                            $aMessageError[] = "L'extension '".$extension_upload."' n'est pas autorisée. Merci de choisir une image avec comme extension 'bmp, gif, jpg, jpeg, png, tiff, webp'.";
                        }
                        else {
                            if (!in_array($typeMime, $autorizedMimeTypes)) {
                                $aMessageError[] = "Le contenu de fichier '".$typeMime."' n'est pas autorisé. Attention Ceci n'est pas une image !";

                            }
                            else {
                                $image_sizes = getimagesize($_FILES['imgRow']['name']);

                                if ($image_sizes[0] > $imageMaxWidth OR $image_sizes[1] > $imageMaxHeight) {
                                    $aMessageError[] = "Taille d'image trop grande. Respectez bien la dimension '64px*64px'.";
                                }
                                elseif ($image_sizes[0] <= $imageMaxWidth && $image_sizes[1] <= $imageMaxHeight) {
                                    move_uploaded_file($_FILES['imgRow']['tmp_name'], $target_dir.$_FILES['imgRow']['name']);
                                    $categoryId   = htmlspecialchars($_POST['categoryId']);
                                    $categoryName = htmlspecialchars($_POST['categoryRow']);

                                    $catgorie = $categoryQuery->findOneById($categoryId);
                                    $catgorie->setCategorie($categoryName);
                                    $catgorie->setImage('/assets/img/icons/map/category/'.$_FILES['imgRow']['name']);
                                    $catgorie->save();

                                    $_SESSION['messageSuccessCategorieUpDated'] = "La catégorie numéro '".$categoryId."' a bien été mise à jour.";
                                    header('Location: /form-category');
                                }
                            }
                        }
                    }
                }
                else {
                    $aMessageError[] = 'Erreur lors du transfert.';
                }
            }
            else {
                $categoryId   = htmlspecialchars($_POST['categoryId']);
                $categoryName = htmlspecialchars($_POST['categoryRow']);

                $catgorie = $categoryQuery->findOneById($categoryId);
                $catgorie->setCategorie($categoryName);
                $catgorie->save();

                $_SESSION['messageSuccessCategorieUpDated'] = "La catégorie numéro '".$categoryId."' a bien été mise à jour.";
                header('Location: /form-category');
            }
        }
    }
}

$smarty->assign('categoryInformations', $categoryInformations);
$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Formulaires/Category/index.html');
?>