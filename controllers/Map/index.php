<?php
// Coder ici avant les $smarty->assign() et le $smarty->display().

$oeuvresQuery 	= new OeuvresQuery();
$oeuvres 		= $oeuvresQuery->find();

$aMapInformations = array();

foreach($oeuvres as $oeuvre) {
	$aMap = array(
		'nom' 		  => $oeuvre->getNom(),
		'artiste' 	  => $oeuvre->getArtiste(),
		'annee' 	  => $oeuvre->getAnnee(),
		'image' 	  => $oeuvre->getImage(),
		'categorie'   => $oeuvre->getCategorie(),
		'description' => $oeuvre->getDescription(),
		'latitude'    => $oeuvre->getLatitude(),
		'longitude'   => $oeuvre->getLongitude()
	);
	$aMapInformations[] = $aMap;
}
$aMapInformations = json_encode($aMapInformations);

$category 	= new CategoryQuery();
$categories = $category->find();

$aCategoriesInformations = array();

foreach($categories as $categorie) {
    $aCategorie = array(
    	'id'		=> $categorie->getId(),
		'categorie'	=> $categorie->getCategorie(),
		'image'		=> $categorie->getImage()
    );
    $aCategoriesInformations[] = $aCategorie;
}
$aCategoriesInformations = json_encode($aCategoriesInformations);

$campus 	= new CampusSiteQuery();
$campuses 	= $campus->find();

$aCampusInformations = array();

foreach($campuses as $campus) {
    $aCampus = array(
		'campus'	=> $campus->getCampus(),
		'latitude'	=>	$campus->getLatitude(),
		'longitude'	=>	$campus->getLongitude(),
    );
    $aCampusesInformations[] = $aCampus;
}
$aCampusesInformations = json_encode($aCampusesInformations);

$smarty->assign('aCategoriesInformations', $aCategoriesInformations);
$smarty->assign('aCampusesInformations', $aCampusesInformations);
$smarty->assign('aMapInformations', $aMapInformations);
$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Map/index.html');
?>
