<?php
function createUser($pseudo, $firstname, $lastname, $password, $passwordCheck, $email) {
	GLOBAL $aMessageError;
	GLOBAL $aMessageSuccess;

	$date 				= date('Y-m-d H:i:s', time());
	$userEmail 			= NULL;
	$userPseudo 		= NULL;
	$pseudoAlreadyInUse = FALSE;
	$emailAlreadyInUse 	= FALSE;

	$usersQuery = new UsersQuery();
	$users 		= $usersQuery->find();

	foreach ($users as $user) {
		$userPseudo = $user->getPseudo();
		$userEmail 	= $user->getEmail();

		if ($pseudo == $userPseudo) {
			$pseudoAlreadyInUse = TRUE;
			$aMessageError[] 	= "Pseudo déjà utilisé, merci d'en choisir un autre.";
		}

		if ($email == $userEmail) {
			$emailAlreadyInUse = TRUE;
			$aMessageError[]   = "Adresse e-mail déjà utilisé, merci d'en choisir une autre.";
		}
	}

	if ($pseudoAlreadyInUse !== TRUE && $emailAlreadyInUse !== TRUE) {
		if ($password != $passwordCheck) {
			$aMessageError[] = "Les mots de passe ne correspondent pas.";
		}
		else {
			$aMessageSuccess[] = "Le nouvel utilisateur a bien été créé.";

			$user = new Users();
			$user->setPseudo($pseudo);
			$user->setPassword(password_hash($password , PASSWORD_DEFAULT));
			$user->setFirstname($firstname);
			$user->setLastname($lastname);
			$user->setEmail($email);
			$user->setRights(1);
			$user->setPasswordChanged(0);
			$user->setDateCreation($date);
			$user->save();
		}
	}

	return $aMessageError;
	return $aMessageSuccess;
}