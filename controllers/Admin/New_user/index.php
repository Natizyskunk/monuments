<?php
include_once('create_user.function.php');

// Le champ du mot de passe est de typepasswordafin d'empêcher la lecture du mot de passe à l'écran par une autre personne. C'est pour cela qu'il est fortement recommandé de demander de saisir à nouveau le mot de passe au cas où le visiteur ait fait une faute de frappe qu'il n'aurait pas pu voir.
if (isset($_POST) && !empty($_POST)) {
	if (isset($_POST['submit'])) {
		if (!empty($_POST['pseudo']) && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['password']) && !empty($_POST['password_check']) && !empty($_POST['email'])) {
			$pseudo 		= htmlspecialchars($_POST['pseudo']);
			$firstname 		= htmlspecialchars($_POST['firstname']);
			$lastname 		= htmlspecialchars($_POST['lastname']);
			$password 		= htmlspecialchars($_POST['password']);
			$passwordCheck 	= htmlspecialchars($_POST['password_check']);
			$email 			= htmlspecialchars($_POST['email']);

			createUser($pseudo, $firstname, $lastname, $password, $passwordCheck, $email);
		}
	}
}

$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Admin/New_user/index.html');
?>