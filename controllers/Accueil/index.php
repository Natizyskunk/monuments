<?php
// Pour affichage aléatoire de 10 images sur le carousel d'accueil
$oeuvresQuery 	= new OeuvresQuery();
$oeuvres 		= $oeuvresQuery->addAscendingOrderByColumn('rand()')->limit(10)->find();

$aOeuvresInformations = array();

if (isset($_SESSION['connectedMessageSuccess'])) {
	$aMessageSuccess[] = $_SESSION['connectedMessageSuccess'];
	unset($_SESSION['connectedMessageSuccess']);
}

foreach($oeuvres as $oeuvre) {
	$aOeuvre = array(
		'nom' 	=> $oeuvre->getNom(),
		'image' => $oeuvre->getImage(),
	);
	$aOeuvresInformations[] = $aOeuvre;
}

$smarty->assign('aOeuvresInformations', $aOeuvresInformations);
$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Accueil/index.html');
?>