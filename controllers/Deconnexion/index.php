<?php
// Nettoyage de la session utilisateur active.
include(_CONFIG_.'session_cleaner.inc.php');

header('Location: /');

$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Deconnexion/index.html');
?>