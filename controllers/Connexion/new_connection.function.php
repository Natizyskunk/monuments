<?php
function newConnection($username, $password) {
    GLOBAL $_SESSION;
    GLOBAL $aMessageError;
    GLOBAL $aMessageSuccess;

    $usersQuery     		= new UsersQuery();
    $user           		= $usersQuery->findOneByPseudo($username);
    $email          		= $usersQuery->findOneByEmail($username);
    $newUserSession 		= FALSE;
    $userId         		= NULL;
    $userPseudo     		= NULL;
    $userPassword   		= NULL;
    $userRights     		= NULL;
    $userPasswordChanged 	= NULL;

    if (!empty($user)) {
        $userId 				= $user->getId();
        $userPseudo 			= $user->getPseudo();
        $userPassword 			= $user->getPassword();
		$userRights    			= $user->getRights();
		$userPasswordChanged 	= $user->getPasswordChanged();

        if (password_verify($password, $userPassword)) {
        // if ($password == $userPassword) {
            $newUserSession = TRUE;
        }
        else {
            $aMessageError[] = "Le mot de passe est erroné. Merci de réessayer.";
        }
    }
    elseif (!empty($email)) {
        $userId 				= $email->getId();
        $userPseudo 			= $email->getPseudo();
        $userPassword 			= $email->getPassword();
		$userRights 			= $email->getRights();
		$userPasswordChanged 	= $email->getPasswordChanged();

        if (password_verify($password, $userPassword)) {
        // if ($password == $userPassword) {
            $newUserSession = TRUE;
        }
        else {
            $aMessageError[] = "Le mot de passe est erroné. Merci de réessayer.";
        }
    }
    else {
        $aMessageError[] = "Aucun utilisateur trouvé avec ce pseudo/email.";
    }

    if ($newUserSession == TRUE) {
        $_SESSION['id'] 						= $userId;
        $_SESSION['pseudo'] 					= $userPseudo;
        $_SESSION['rights'] 					= $userRights;
		$_SESSION['isConnected'] 				= TRUE;
		$_SESSION['userPasswordChanged']		= $userPasswordChanged;
        $_SESSION['connectedMessageSuccess'] 	= 'Bonjour '.$userPseudo.', nous somme très heureux de vous revoir parmis nous!';

		//! Rajouter un pop-up prévenant de la présence et demandant l'autorisation de l'utilisation des cookies.
		//! Expliquer que les cookies sont importants pour la vie du site et pour avoir une navigation optimisée.
		// setcookie('login', $userPseudo);
		// setcookie('pass_hash', $userPassword);
    }

    return $_SESSION;
    return $aMessageError;
    return $aMessageSuccess;
}