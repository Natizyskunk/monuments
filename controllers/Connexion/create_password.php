<?php
// Coder ici avant les $smarty->assign() et le $smarty->display().

if ($_SESSION['userPasswordChanged'] == 1) {
	header('Location: /');
}
else {
	// create_password($password, $passwordCheck);

	$date 	= date('Y-m-d H:i:s', time());
	$pseudo = $_SESSION['pseudo'];
	$password;
	$passwordCheck;

	$usersQuery = new UsersQuery();
	$user 		= $usersQuery->findOneByPseudo($pseudo);

	if (!empty($users)) {
		if ($password != $passwordCheck) {
			$aMessageError[] = "Les mots de passe ne correspondent pas.";
		}
		else {
			$aMessageSuccess[] = "Le nouveau mot de passe a bien été défini.";

			$user->setPassword(password_hash($password , PASSWORD_DEFAULT));
			$user->save();
		}
	}
	else {
		$aMessageError[] = "Une erreur s'est produite lors de la recherche del'utilisateur. <br>
			Merci de réessayer dans quelques isntants. <br>
			Si l'erreur persiste, merci de contacter l'administrateur du site web.";
	}
}

$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Connexion/create_password.html');
?>