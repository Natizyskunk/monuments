<?php
// Coder ici avant les $smarty->assign() et le $smarty->display().
include_once('new_connection.function.php');

// Le champ du mot de passe est de type password afin d'empêcher la lecture du mot de passe à l'écran par une autre personne. C'est pour cela qu'il est fortement recommandé de demander de saisir à nouveau le mot de passe au cas où le visiteur ait fait une faute de frappe qu'il n'aurait pas pu voir.
if (isset($_POST) && !empty($_POST)) {
	if (isset($_POST['submit'])) {
		$username = htmlspecialchars($_POST['username']);
		$password = htmlspecialchars($_POST['password']);

		newConnection($username, $password);

		if ($_SESSION['userPasswordChanged'] == 1) {
			header('Location: /');
		}
		else {
			header('Location: /create-password');
		}
	}
}

$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Connexion/index.html');
?>