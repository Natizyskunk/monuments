<?php
if (isset($_SESSION['NewOeuvreMessageSuccess'])) {
	$aMessageSuccess[] = $_SESSION['NewOeuvreMessageSuccess'];
	unset($_SESSION['NewOeuvreMessageSuccess']);
}
if (isset($_SESSION['messageSuccessOeuvreEdit'])) {
	$aMessageSuccess[] = $_SESSION['messageSuccessOeuvreEdit'];
	unset($_SESSION['messageSuccessOeuvreEdit']);
}

$maxFileSize  = 5000000; // 1.5Mo.
$oeuvresQuery = new OeuvresQuery();

// Pour la pagination.
$page = 1;
if (isset($_GET['pagination'])) {
	$page = $_GET['changePage'];
}

// Pour choisir le nombre d'oeuvres par page.
$maxPerPage = 12;
if (isset($_GET['6'])) {
	$maxPerPage = $_GET['6PerPage'];
}
else if (isset($_GET['12'])) {
	$maxPerPage = $_GET['12PerPage'];
}
else if (isset($_GET['18'])) {
	$maxPerPage = $_GET['18PerPage'];
}
else if (isset($_GET['perPage'])) {
	$maxPerPage = $_GET['perPage'];
}
else if (isset($_GET['maxItems'])) {
	$maxPerPage = $_GET['maxItems'];
}

$oeuvres = $oeuvresQuery->paginate($page, $maxPerPage);
$links 	 = $oeuvres->getLinks(5);

$aOeuvresInformations = array();

// Fonction qui crée une liste provenant d'une requette.
function listCreatorSimple($oeuvres, $case, $filter) {
	foreach ($oeuvres as $oeuvre) {
		$aOeuvres = array(
			'id'		  	=> $oeuvre->getId(),
			'nom' 		  	=> $oeuvre->getNom(),
			'artiste' 	  	=> $oeuvre->getArtiste(),
			'annee' 	  	=> $oeuvre->getAnnee(),
			'categorie'	  	=> $oeuvre->getCategorie(),
			'image' 	  	=> $oeuvre->getImage(),
			'description' 	=> $oeuvre->getDescription(),
			'latitude'    	=> $oeuvre->getLatitude(),
			'longitude'   	=> $oeuvre->getLongitude(),
			'campus'      	=> $oeuvre->getCampus(),
			'droitsAuteur' 	=> $oeuvre->getDroitsAuteur()
		);

		if ($case == 0) {
			$aOeuvresInformations[] = $aOeuvres;
		}
		else if ($case == 1) {
			if ($aOeuvres['campus'] == $filter + 1) {
				$aOeuvresInformations[] = $aOeuvres;
			}
		}
		else if ($case == 2) {
			if ($aOeuvres['categorie'] == $filter + 1) {
				$aOeuvresInformations[] = $aOeuvres;
			}
		}
	}

	if (isset($aOeuvresInformations)) {
		return $aOeuvresInformations;
	}
}

// Pour recherche d'une oeuvre
if (isset($_GET['searchButton'])) {
	$oeuvres  = $oeuvresQuery->find();

	foreach ($oeuvres as $oeuvre) {
		$name = $oeuvre->getNom();

		if (strpos(strtoupper($name), strtoupper($_GET['searchInput'])) !== FALSE) {
			$aOeuvres = array(
				'id'		  => $oeuvre->getId(),
				'nom' 		  => $oeuvre->getNom(),
				'artiste' 	  => $oeuvre->getArtiste(),
				'annee' 	  => $oeuvre->getAnnee(),
				'categorie'	  => $oeuvre->getCategorie(),
				'image' 	  => $oeuvre->getImage(),
				'description' => $oeuvre->getDescription(),
				'latitude'    => $oeuvre->getLatitude(),
				'longitude'   => $oeuvre->getLongitude(),
				'campus'      => $oeuvre->getCampus(),
				'droitsAuteur' 	=> $oeuvre->getDroitsAuteur()
			);
			$aOeuvresInformations[] = $aOeuvres;
		}
	}
}
else {
	$aOeuvresInformations = listCreatorSimple($oeuvres, 0, 0);

	if (isset($_GET['categoryFilter']) || isset($_GET['campusFilter']) || isset($_GET['categorySecondFilter']) || isset($_GET['campusSecondFilter'])) {
		if (!isset($_GET['allCampus']) || !isset($_GET['allCategories'])) {
			// Chaine de conditions qui à comme but de detecter les GET actifs et les passer comme variables
			if (isset($_GET['campusFilter'])) {
				$campusFilter = $_GET['campusFilter'];
			}

			if (isset($_GET['campusSecondFilter'])) {
				$campusFilter = $_GET['campusSecondFilter'];
			}

			if (isset($_GET['categoryFilter'])) {
				$categoryFilter = $_GET['categoryFilter'];
			}

			if (isset($_GET['categorySecondFilter'])) {
				$categoryFilter = $_GET['categorySecondFilter'];
			}

			// Pour filtrer par campus
			if (isset($_GET['campusFilter']) && !isset($_GET['categorySecondFilter']) && !isset($_GET['campusSecondFilter'])) {
				$oeuvresFilterByCampus 	= $oeuvresQuery->filterByCampus($_GET['campusFilter'] + 1);
				$oeuvres 				= $oeuvresFilterByCampus->find();
				$aOeuvresInformations 	= listCreatorSimple($oeuvres, 0, null);
			}

			// Pour filtrer par catégorie
			if (isset($_GET['categoryFilter']) && !isset($_GET['campusSecondFilter']) && !isset($_GET['categorySecondFilter'])) {
				$oeuvresFilterByCategorie = $oeuvresQuery->filterByCategorie($_GET['categoryFilter'] + 1);
				$oeuvres 				  = $oeuvresFilterByCategorie->find();
				$aOeuvresInformations 	  = listCreatorSimple($oeuvres, 0, null);
			}

			// Pour filtrer par campus -> catégorie
			if (isset($_GET['categoryFilter']) && isset($_GET['campusSecondFilter']) && !isset($_GET['categorySecondFilter'])) {
				$oeuvresFilterByCategorie 	= $oeuvresQuery->filterByCategorie($_GET['categoryFilter'] + 1);
				$oeuvres 					= $oeuvresFilterByCategorie->find();
				$aOeuvresInformations 		= listCreatorSimple($oeuvres, 1, $_GET['campusSecondFilter']);
			}

			// Pour filtrer par catégorie -> campus
			if (isset($_GET['campusFilter']) && isset($_GET['categorySecondFilter'])) {
				$oeuvresFilterByCampus 	= $oeuvresQuery->filterByCampus($_GET['campusFilter'] + 1);
				$oeuvres 				= $oeuvresFilterByCampus->find();
				$aOeuvresInformations 	= listCreatorSimple($oeuvres, 2, $_GET['categorySecondFilter']);
			}

			// Pour filtrer par campus -> catégorie suite à une selection de pagination
			if (isset($_GET['campusSecondFilter']) && isset($_GET['categorySecondFilter'])) {
				$oeuvresFilterByCampus 	= $oeuvresQuery->filterByCampus($_GET['campusSecondFilter'] + 1);
				$oeuvres 				= $oeuvresFilterByCampus->find();
				$aOeuvresInformations 	= listCreatorSimple($oeuvres, 2, $_GET['categorySecondFilter']);
			}
		}
	}
}

// Aucun filtre selectionné
if (!isset($_GET['searchButton'])) {
	if (isset($_GET['allCampus']) && isset($_GET['categorySecondFilter'])) {
		$aOeuvresInformations = listCreatorSimple($oeuvres, 2, $_GET['categorySecondFilter']);
	}
	else if (isset($_GET['allCampus']) && !isset($_GET['categorySecondFilter'])) {
		$aOeuvresInformations = listCreatorSimple($oeuvres, 0, 0);
	}

	if (isset($_GET['allCategories']) && isset($_GET['campusSecondFilter'])) {
		$aOeuvresInformations = listCreatorSimple($oeuvres, 1, $_GET['campusSecondFilter']);
	}
	else if (isset($_GET['allCampus']) && !isset($_GET['campusSecondFilter'])) {
		$aOeuvresInformations = listCreatorSimple($oeuvres, 0, 0);
	}
}

// Pour tri selon la catégorie
$category_data 	= new CategoryQuery();
$categories 	= $category_data->find();

$aCategoriesInformations = array();

foreach($categories as $categorie) {
    $aCategories = array(
        'id'			=> $categorie->getId(),
		'category'		=> $categorie->getCategorie()
    );
    $aCategoriesInformations[] = $aCategories;
}

// Pour tri selon la localisation
$campusData = new CampusSiteQuery();
$campus 	= $campusData->find();

$aCampusInformations = array();

foreach ($campus as $campus) {
	$aCampus = array(
		'id' 	 => $campus->getId(),
		'campus' => $campus->getCampus()
	);
	$aCampusInformations[] = $aCampus;
}

// Pour suppression des oeuvres
if (isset($_POST['supprButton'])) {
	$monOeuvre 	= htmlspecialchars($_POST['editModalLabel']);
	$oeuvre 	= $oeuvresQuery->findOneByNom($monOeuvre);
	$monImage 	= $oeuvre->getImage();
	unlink(_PATH_.$monImage);
	$oeuvre->delete();

	$aMessageSuccess[] = "SUCCESS!";
	header('Location: /patrimoine');
}

// Pour modification des oeuvres
if (isset($_POST['updateDescriptionButton'])) {
	$upToDateDescription 	= htmlspecialchars($_POST['upToDateDescription']);
	$oeuvreId 			 	= htmlspecialchars($_POST['oeuvreId']);
	$upToDatedroitsAuteur 	= htmlspecialchars($_POST['upToDatedroitsAuteur']);
	$oeuvre 			 	= $oeuvresQuery->findOneById($oeuvreId);
	$name				 	= $oeuvre->getNom();

	if (!empty($_POST['upToDateDescription']) && !empty($_POST['upToDatedroitsAuteur'])) {
		if (!empty($_FILES['fileUser']['name'])) {
			if ($_FILES['fileUser']['error'] == 0) {
				if ($_FILES['fileUser']['size'] > $maxFileSize) {
					$aMessageError[] = "L'image ne doit pas dépasser 5Mo.";
				}
				else {
					$infosfichier           = pathinfo($_FILES['fileUser']['name']);
					$typeMime               = mime_content_type($_FILES['fileUser']['tmp_name']);
					$extension_upload       = $infosfichier['extension'];
					$target_dir             = _ASSETS_."img/photos/oeuvres/";
					$extensions_autorisees  = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'tiff', 'webp');
					$autorizedMimeTypes     = array(
						"image/bmp",
						"image/gif",
						"image/jpg",
						"image/jpeg",
						"image/png",
						"image/tiff",
						"image/webp"
					);

					if (!in_array($extension_upload, $extensions_autorisees)) {
						$aMessageError[] = "L'extension '".$extension_upload."' n'est pas autorisée. Merci de choisir une image avec comme extension 'bmp, gif, jpg, jpeg, png, tiff, webp'.";
					}
					else {
						if (!in_array($typeMime, $autorizedMimeTypes)) {
							$aMessageError[] = "Le contenu de fichier '".$typeMime."' n'est pas autorisé. Attention Ceci n'est pas une image !";
						}
						else {
							move_uploaded_file($_FILES['fileUser']['tmp_name'], $target_dir.$_FILES['fileUser']['name']);

							$image 	= $oeuvre->getImage();
							unlink(_PATH_.$image);

							$oeuvre->setDescription($upToDateDescription);
							$oeuvre->setDroitsAuteur($upToDatedroitsAuteur);
							$oeuvre->setImage('/assets/img/photos/oeuvres/'.$_FILES['fileUser']['name']);
							$oeuvre->save();

							// $aMessageSuccess = "L'oeuvre '".$name."' à bien été modifiée.";
							$_SESSION['messageSuccessOeuvreEdit'] = "L'oeuvre '".$name."' à bien été modifiée.";

							header('Location: /patrimoine');
						}
					}
				}
			}
			else {
				$aMessageError[] = 'Erreur lors du transfert.';
			}
		}
		else {
			$oeuvre->setDescription($upToDateDescription);
			$oeuvre->setDroitsAuteur($upToDatedroitsAuteur);
			$oeuvre->save();

			// $aMessageSuccess = "L'oeuvre '".$name."' à bien été modifiée.";
			$_SESSION['messageSuccessOeuvreEdit'] = "L'oeuvre '".$name."' à bien été modifiée.";

			header('Location: /patrimoine');
		}
	}
	else {
		$aMessageError[] = 'Le formulaire doit être complet pour être soumis.';
	}
}

if (isset($campusFilter) || isset($campusSecondFilter) ) {
	$smarty->assign('campusFilter', $campusFilter);
}

if (isset($categoryFilter) || isset($categorySecondFilter)) {
	$smarty->assign('categoryFilter', $categoryFilter);
}

$smarty->assign('maxPerPage', $maxPerPage);
$smarty->assign('links', $links);
$smarty->assign('aCampusInformations', $aCampusInformations);
$smarty->assign('aCategoriesInformations', $aCategoriesInformations);
$smarty->assign('aOeuvresInformations', $aOeuvresInformations);
$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Patrimoine/index.html');
?>
