<?php
$idOeuvre = $matches[1];
// $aOeuvresInformations = array();
$aOeuvre = array();

// Pour recherche d'une oeuvre
if (isset($idOeuvre)) {
	$oeuvresQuery = new OeuvresQuery();
	$oeuvre 	  = $oeuvresQuery->findPk($idOeuvre);
	$name 		  = $oeuvre->getNom();

	$aOeuvre = array(
			'id'		  => $oeuvre->getId(),
			'nom' 		  => $oeuvre->getNom(),
			'artiste' 	  => $oeuvre->getArtiste(),
			'annee' 	  => $oeuvre->getAnnee(),
			'categorie'	  => $oeuvre->getCategorie(),
			'image' 	  => $oeuvre->getImage(),
			'description' => $oeuvre->getDescription(),
			'latitude'    => $oeuvre->getLatitude(),
			'longitude'   => $oeuvre->getLongitude(),
			'campus'      => $oeuvre->getCampus()
		);

		// $aOeuvreInformations[] = $aOeuvre;
}

$smarty->assign('idOeuvre', $idOeuvre);
// $smarty->assign('aOeuvreInformations', $aOeuvreInformations);
$smarty->assign('aOeuvre', $aOeuvre);
$smarty->assign('aMessageSuccess', $aMessageSuccess);
$smarty->assign('aMessageError', $aMessageError);
$smarty->display(_TPL_ . 'Patrimoine/oeuvre_select.html');
?>
