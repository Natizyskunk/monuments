@echo off
TITLE Monuments project installation script
set composerExe=bin\composer\composer.bat
set cliReplace=bin\php-cli-replace\replace.php
set propelExe=vendor\bin\propel.bat
set port=8000

echo.
echo    ************************************************************
echo   *                                                            *
echo   *          - Monuments_ADMIN Installation script -           *
echo   *                                                            *
echo   *       This script will install all the requirements        *
echo   *             before so you can run the project.             *
echo   *                                                            *
echo   *    It gonna install: Chocolatey, Composer and Node.js.     *
echo   *  It'll also import the database and configure propel ORM.  *
echo   *                                                            *
echo    ************************************************************
TIMEOUT /T 10

cls && COLOR 0C
REM Chocolatey + Node.js installation.
echo Please start the 'start-win-chocolatey+nodejs-install.bat' script as Administrator to install Chocolatey.
echo Then come back here and hit next.
PAUSE

cls && COLOR 0F
REM Composer installation.
echo Installation of latest version of composer.
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=bin/composer
php -r "unlink('composer-setup.php');"

REM Composer and Node.js packages installation.
echo Installation off all necessary composer and node.js packages.
cmd.exe /c %composerExe% install
cmd.exe /c npm install

REM Auto-feel propel.php with db credentials.
echo Auto-feeling propel.php file with db credentials.
php %cliReplace%

REM Update of propel ORM config.
echo Updating config for propel ORM.
cmd.exe /c %propelExe% sql:build --overwrite
cmd.exe /c %propelExe% model:build
cmd.exe /c %composerExe% dump-autoload
cmd.exe /c %propelExe% config:convert

REM Importation of databse via propel ORM migrations.
echo We will now import the database via propel ORM migrations.
cmd.exe /c %propelExe% migrate

cls && COLOR 0C
echo Installation and configuration are now finished you.
echo.
echo Please note that before accessing the website, you need to start your Apache, PHP and MySQL servers/apps.
echo To do this you can start the 'start-win-php-server.bat' script to launch your internal development php server
echo and you also run the 'start-win-npm-sass.bat' script if you're doing scss styling to enable auto compiling.
echo.
PAUSE