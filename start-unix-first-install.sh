#!/bin/bash
readonly EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
readonly ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"
readonly propelExe=vendor/bin/propel
readonly composerExe=bin/composer/unix/composer

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
else
	### Download and install Composer.
	php composer-setup.php --install-dir=bin/composer/unix --filename=composer
	rm composer-setup.php

	### Download and install nodeJs.
	curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
	sudo apt-get install -y nodejs

	### Install all necessary composer packages.
	$composerExe install

	### Install all necessary node.js packages.
	npm install

	### Create propel ORM config.
	# propel sql:build --overwrite
	# propel model:build
	# composer dump-autoload
	$propelExe config:convert

	echo ""
	echo "Please now start your internal development php server with the 'start-unix-php-server.sh' script."
	echo "You can also run the 'start-win-npm-sass.bat' script if you're doing scss styling."
fi